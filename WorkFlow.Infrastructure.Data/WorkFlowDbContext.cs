﻿using Microsoft.EntityFrameworkCore;
using System;
using WorkFlow.Services.Models;

namespace WorkFlow.Infrastructure.Data
{
    public class WorkFlowDbContext : DbContext
    {
        public WorkFlowDbContext()
        {
        }

        public WorkFlowDbContext(DbContextOptions options) : base(options)
        { }
        public DbSet<AssignmentRule> AssignmentRules { get; set; }
        public DbSet<CheckList> CheckLists { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<MailModule> MailModules { get; set; }
        public DbSet<MailTemplate> MailTemplates { get; set; }
        public DbSet<PersonalProfile> PersonalProfiles { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<Property> Properties { get; set; }
        public DbSet<PropertyChoice> PropertyChoices { get; set; }
        public DbSet<PropertyDefaultValueType> PropertyDefaultValueTypes { get; set; }
        public DbSet<PropertyDefaultVariable> PropertyDefaultVariables { get; set; }
        public DbSet<PropertySetting> PropertySettings { get; set; }
        public DbSet<SubContidition> SubContiditions { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Workflow> Workflows { get; set; }
        public DbSet<WorkflowCategory> WorkflowCategories { get; set; }
        public DbSet<WorkflowCondition> WorkflowConditions { get; set; }
        public DbSet<WorkflowStep> WorkflowSteps { get; set; }
        public DbSet<WorkingTime> WorkingTimes { get; set; }
        public DbSet<Workweek> Workweeks { get; set; }
        public DbSet<SupportContact> SupportContacts { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
                    => optionsBuilder.LogTo(Console.WriteLine);
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<AssignmentRule>().ToTable("AssignmentRule");
            builder.Entity<CheckList>().ToTable("CheckList");
            builder.Entity<Department>().ToTable("Department");
            builder.Entity<DocumentType>().ToTable("DocumentType");
            builder.Entity<ErrorLog>().ToTable("ErrorLog");
            builder.Entity<Group>().ToTable("Group");
            builder.Entity<MailModule>().ToTable("MailModule");
            builder.Entity<MailTemplate>().ToTable("MailTemplate");
            builder.Entity<PersonalProfile>().ToTable("PersonalProfile");
            builder.Entity<Position>().ToTable("Position");
            builder.Entity<Property>().ToTable("Property");
            builder.Entity<PropertyChoice>().ToTable("PropertyChoice");
            builder.Entity<PropertyDefaultValueType>().ToTable("PropertyDefaultValueType");
            builder.Entity<PropertyDefaultVariable>().ToTable("PropertyDefaultVariable");
            builder.Entity<PropertySetting>().ToTable("PropertySetting");
            builder.Entity<SubContidition>().ToTable("SubContidition");
            builder.Entity<Supplier>().ToTable("Supplier");
            builder.Entity<Workflow>().ToTable("Workflow");
            builder.Entity<WorkflowCategory>().ToTable("WorkflowCategory");
            builder.Entity<WorkflowCondition>().ToTable("WorkflowCondition");
            builder.Entity<WorkflowStep>().ToTable("WorkflowStep");
            builder.Entity<Workweek>().ToTable("Workweek");
            builder.Entity<SupportContact>().ToTable("SupportContact");
        }
    }
}
