﻿using System.ComponentModel;

namespace WorkFlow.Services.Models
{
    public enum WorkflowStatus
    {
        [Description("Deactive")]
        Deactive,
        [Description("Active")]
        Active,
        [Description("Draft")]
        Draft
    }
}
