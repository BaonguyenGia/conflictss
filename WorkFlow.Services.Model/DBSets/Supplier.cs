﻿using System.ComponentModel.DataAnnotations;

namespace WorkFlow.Services.Models
{
    public class Supplier : EngineEntity
    {
        public string Title { get; set; }
        public string Name { get; set; }
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string TaxCode { get; set; }
        public string BankAccount { get; set; }
        public string BankName { get; set; }
    }
}
