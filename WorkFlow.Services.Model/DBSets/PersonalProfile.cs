﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class PersonalProfile : EngineEntity
    {
        public string AccountId { get; set; }
        [Required]
        public string AccountName { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        [ForeignKey("Department")]
        [Required]
        public long DepartmentId { get; set; }
        [ForeignKey("Manager")]
        public long? ManagerId { get; set; }
        [ForeignKey("Position")]
        public long? PositionId { get; set; }
        public bool Gender { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Address { get; set; }
        public string StaffId { get; set; }
        public DateTime? DateOfHire { get; set; }
        public string Mobile { get; set; }
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string Email { get; set; }
        public string ImagePath { get; set; }
        public string SignatureUserName { get; set; }
        public string SignaturePassword { get; set; }
        public string SignatureTitle { get; set; }
        public string SignatureEmail { get; set; }
        public bool EnableSignature { get; set; }
        public int? UserStatus { get; set; }
        #region Foreign
        public Department Department { get; set; }
        public PersonalProfile Manager { get; set; }
        public Position Position { get; set; }
        public ICollection<Group> Groups { get; set; }
        #endregion
    }
}
