﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WorkFlow.Services.Models
{
    public class PropertyDefaultVariable : EngineEntity
    {
        public string Name { get; set; }
        public long? DefaultTypeId { get; set; }
        #region Foreign
        [ForeignKey("DefaultTypeId")]
        public PropertyDefaultValueType DefaultType { get; set; }
        #endregion
    }
}
