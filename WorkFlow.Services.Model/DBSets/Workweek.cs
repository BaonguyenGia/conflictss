﻿using System.ComponentModel.DataAnnotations;

namespace WorkFlow.Services.Models
{
    public class Workweek : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public bool IsFullTime { get; set; }
    }
}
