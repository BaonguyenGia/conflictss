﻿using System.ComponentModel.DataAnnotations;

namespace WorkFlow.Services.Models
{
    public class WorkflowCategory : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public int Order { get; set; }
    }
}
