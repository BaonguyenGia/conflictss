﻿using System.ComponentModel.DataAnnotations;

namespace WorkFlow.Services.Models
{
    public class DocumentType : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
