﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkweekService
    {
        Task<ApiResponse<WorkweekResource>> CreateWorkweek(WorkweekResource workweekResource);
        Task<ApiResponse<WorkweekResource>> UpdateWorkweek(long id, WorkweekResource workweekResource);
        Task<ApiResponse<WorkweekResource>> DeleteWorkweek(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkweekResource>> GetWorkweek(long id);
        Task<ApiResponse<QueryResultResource<WorkweekResource>>> GetWorkweeks(QueryResource queryObj);
    }
}
