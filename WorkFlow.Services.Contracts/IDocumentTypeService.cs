﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IDocumentTypeService
    {
        Task<ApiResponse<DocumentTypeResource>> CreateDocumentType(DocumentTypeResource documentTypeResource);
        Task<ApiResponse<DocumentTypeResource>> UpdateDocumentType(long id, DocumentTypeResource documentTypeResource);
        Task<ApiResponse<DocumentTypeResource>> DeleteDocumentType(long id, bool removeFromDB = false);
        Task<ApiResponse<DocumentTypeResource>> GetDocumentType(long id);
        Task<ApiResponse<QueryResultResource<DocumentTypeResource>>> GetDocumentTypes(QueryResource queryObj);
        Task<ApiResponse<IList<DocumentTypeResource>>> ImportFromList(List<string> documentTypeTitles);
    }
}
