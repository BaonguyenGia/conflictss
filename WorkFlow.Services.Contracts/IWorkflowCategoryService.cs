﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkflowCategoryService
    {
        Task<ApiResponse<WorkflowCategoryResource>> CreateWorkflowCategory(WorkflowCategoryResource WorkflowCategoryResource);
        Task<ApiResponse<WorkflowCategoryResource>> UpdateWorkflowCategory(long id, WorkflowCategoryResource WorkflowCategoryResource);
        Task<ApiResponse<WorkflowCategoryResource>> DeleteWorkflowCategory(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkflowCategoryResource>> GetWorkflowCategory(long id);
        Task<ApiResponse<QueryResultResource<WorkflowCategoryResource>>> GetWorkflowCategories(QueryResource queryObj);
    }
}
