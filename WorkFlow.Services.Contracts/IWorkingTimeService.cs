﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkingTimeService
    {
        Task<ApiResponse<WorkingTimeResource>> CreateWorkingTime(WorkingTimeResource workingTimeResource);
        Task<ApiResponse<WorkingTimeResource>> UpdateWorkingTime(long id, WorkingTimeResource workingTimeResource);
        Task<ApiResponse<WorkingTimeResource>> DeleteWorkingTime(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkingTimeResource>> GetWorkingTime(long id);
        Task<ApiResponse<QueryResultResource<WorkingTimeResource>>> GetWorkingTimes(QueryResource queryObj);
    }
}
