﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IWorkflowService
    {
        Task<ApiResponse<WorkflowResource>> CreateWorkflow(WorkflowResource workflowResource);
        Task<ApiResponse<WorkflowResource>> UpdateWorkflow(long id, WorkflowResource workflowResource);
        Task<ApiResponse<WorkflowResource>> DeleteWorkflow(long id, bool removeFromDB = false);
        Task<ApiResponse<WorkflowResource>> GetWorkflow(long id);
        Task<ApiResponse<QueryResultResource<WorkflowResource>>> GetWorkflows(QueryResource queryObj);
        Task<ApiResponse<WorkflowResource>> UploadWorkflowImage(long id, IFormFile file);
        Task<ApiResponse<FileStream>> GetWorkflowImage(long id);
        Task<ApiResponse<WorkflowResource>> UploadWorkflowImageBase64(long id, string base64String);
    }
}
