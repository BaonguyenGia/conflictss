﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IPersonalProfileService
    {
        Task<ApiResponse<PersonalProfileResource>> CreatePersonalProfile(PersonalProfileResource personalProfileResource);
        Task<ApiResponse<PersonalProfileResource>> UpdatePersonalProfile(long id, PersonalProfileResource personalProfileResource);
        Task<ApiResponse<PersonalProfileResource>> DeletePersonalProfile(long id, bool removeFromDB = false);
        Task<ApiResponse<PersonalProfileResource>> GetPersonalProfile(long id);
        Task<ApiResponse<QueryResultResource<PersonalProfileResource>>> GetPersonalProfiles(QueryResource queryObj);
        Task SecretUpdatePersonalProfile();
        Task<ApiResponse<PersonalProfileResource>> UploadPersonalProfileAvatar(long id, IFormFile file);
        Task<ApiResponse<FileStream>> GetPersonalProfileAvatar(long id);
        Task<ApiResponse<FileStream>> GetPersonalProfileAvatar(string email);
        Task<ApiResponse<PersonalProfileResource>> UploadPersonalProfileAvatarBase64(long id, string base64String);
    }
}
