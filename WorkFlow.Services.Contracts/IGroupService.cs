﻿using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;

namespace WorkFlow.Services.Contracts
{
    public interface IGroupService
    {
        Task<ApiResponse<GroupResource>> CreateGroup(GroupResource GroupResource);
        Task<ApiResponse<GroupResource>> UpdateGroup(long id, GroupResource GroupResource);
        Task<ApiResponse<GroupResource>> DeleteGroup(long id, bool removeFromDB = false);
        Task<ApiResponse<GroupResource>> GetGroup(long id);
        Task<ApiResponse<QueryResultResource<GroupResource>>> GetGroups(QueryResource queryObj);
    }
}
