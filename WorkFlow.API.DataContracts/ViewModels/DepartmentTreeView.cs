using System.Collections.Generic;

namespace WorkFlow.API.DataContracts
{
    public class DepartmentTreeView
    {
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public string Label { get; set; }
        public List<DepartmentTreeView> Items { get; set; }
    }
}
