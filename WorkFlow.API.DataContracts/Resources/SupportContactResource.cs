using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class SupportContactResource : EngineEntity
    {
        public string Title { get; set; }
        public string PhoneNumber { get; set; }
        public int Order { get; set; }
        public string PersonalProfileName { get; set; }
        [Required]
        public long PersonalProfileId { get; set; }
        public PersonalProfile PersonalProfile { get; set; }
    }
}
