﻿using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class DocumentTypeResource : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
