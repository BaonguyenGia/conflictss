﻿using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class WorkingTimeResource : EngineEntity
    {
        [Required]
        public string Title { get; set; }
        public long? WorkweekId { get; set; }
        public double MorningStart { get; set; }
        public double MorningEnd { get; set; }
        public double AfternoonStart { get; set; }
        public double AfternoonEnd { get; set; }
    }
}
