﻿using System;
using System.ComponentModel.DataAnnotations;
using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class PersonalProfileResource : EngineEntity
    {
        public string AccountId { get; set; }
        [Required]
        public string AccountName { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        [Required]
        public long DepartmentId { get; set; }
        public string DepartmentTitle { get; set; }
        public long? ManagerId { get; set; }
        public long? PositionId { get; set; }
        public string PositionTitle { get; set; }
        public bool Gender { get; set; }
        public DateTime? BirthDay { get; set; }
        public string Address { get; set; }
        public string StaffId { get; set; }
        public DateTime? DateOfHire { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string ImagePath { get; set; }
        public string SignatureUserName { get; set; }
        public string SignaturePassword { get; set; }
        public string SignatureTitle { get; set; }
        public string SignatureEmail { get; set; }
        public bool EnableSignature { get; set; }
        public int? UserStatus { get; set; }
    }
}
