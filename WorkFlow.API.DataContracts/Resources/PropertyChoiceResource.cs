﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts
{
    public class PropertyChoiceResource : EngineEntity
    {
        public string Title { get; set; }
        public long SettingId { get; set; }
    }
}
