﻿using WorkFlow.Services.Models;

namespace WorkFlow.API.DataContracts.Resources
{
    public class QueryResource
    {
        public string SortBy { get; set; }
        public bool IsSortAscending { get; set; }
        public int? Page { get; set; }
        public byte PageSize { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string AccountName { get; set; }
        public WorkflowStatus? Status { get; set; }
        public long? CategoryId { get; set; }
        public long? DepartmentId { get; set; }
        public long? PositionId { get; set; }
        public long? ManagerId { get; set; }
        public long? GroupId { get; set; }
        public string CreatePermission { get; set; }
        public string SeenPermission { get; set; }
        public string FullNameOrEmail { get; set; }
    }
}