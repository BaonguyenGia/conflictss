﻿using AutoMapper;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Models;

namespace WorkFlow.IoC.Configuration.AutoMapper.Profiles
{
    public class APIMappingProfile : Profile
    {
        public APIMappingProfile()
        {
            CreateMap<Department, DepartmentResource>()
                .ForMember(dr => dr.Label, opt => opt.MapFrom(d => d.Title));
            CreateMap<DepartmentResource, Department>()
                .ForMember(d => d.ParentId, opt => opt.MapFrom(dr => dr.ParentId == 0 ? null : dr.ParentId));

            CreateMap<PersonalProfile, PersonalProfileResource>()
                .ForMember(dr => dr.PositionTitle, opt => opt.MapFrom(d => d.Position.Title))
                .ForMember(dr => dr.DepartmentTitle, opt => opt.MapFrom(d => d.Department.Title));
            CreateMap<PersonalProfileResource, PersonalProfile>();

            CreateMap<Workflow, WorkflowResource>()
                .ForMember(dr => dr.CategoryName, opt => opt.MapFrom(d => d.Category.Title));
            CreateMap<WorkflowResource, Workflow>();

            CreateMap<Position, PositionResource>().ReverseMap();
            CreateMap<WorkflowCategory, WorkflowCategoryResource>().ReverseMap();
            CreateMap<DocumentType, DocumentTypeResource>().ReverseMap();
            CreateMap<Workweek, WorkweekResource>().ReverseMap();
            CreateMap<MailTemplate, MailTemplateResource>().ReverseMap();
            CreateMap<MailModule, MailModuleResource>().ReverseMap();
            CreateMap<WorkflowStep, WorkflowStepResource>().ReverseMap();
            CreateMap<AssignmentRule, AssignmentRuleResource>().ReverseMap();
            CreateMap<CheckList, CheckListResource>().ReverseMap();
            CreateMap<DocumentType, DocumentTypeResource>().ReverseMap();
            CreateMap<Property, PropertyResource>().ReverseMap();
            CreateMap<PropertyDefaultValueType, PropertyDefaultValueTypeResource>().ReverseMap();
            CreateMap<PropertySetting, PropertySettingResource>().ReverseMap();
            CreateMap<PropertyChoice, PropertyChoiceResource>().ReverseMap();
            CreateMap<WorkflowCondition, WorkflowConditionResource>().ReverseMap();
            CreateMap<SubContidition, SubContiditionResource>().ReverseMap();
            CreateMap<SupportContact, SupportContactResource>()
                      .ForMember(dr => dr.PersonalProfileName, opt => opt.MapFrom(d => d.PersonalProfile.FullName));
            CreateMap<SupportContactResource, SupportContact>();
            CreateMap<WorkingTime, WorkingTimeResource>().ReverseMap();
            CreateMap(typeof(QueryResult<>), typeof(QueryResultResource<>));
        }
    }
}
