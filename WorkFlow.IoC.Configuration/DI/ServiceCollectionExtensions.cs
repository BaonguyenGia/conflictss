﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.WsFederation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using WorkFlow.Infrastructure.Data;
using WorkFlow.Services;
using WorkFlow.Services.Contracts;
using WorkFlow.Tools.Encryptions;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.IoC.Configuration.DI
{
    public static class ServiceCollectionExtensions
    {
        public static void ConfigureBusinessServices(this IServiceCollection services, IConfiguration configuration)
        {
            if (services != null)
            {
                services.AddTransient<IPersonalProfileService, PersonalProfileService>();
                services.AddTransient<IDocumentTypeService, DocumentTypeService>();
                services.AddTransient<IGroupService, GroupService>();
                services.AddTransient<IMailModuleService, MailModuleService>();
                services.AddTransient<IMailTemplateService, MailTemplateService>();
                services.AddTransient<IDepartmentService, DepartmentService>();
                services.AddTransient<IPositionService, PositionService>();
                services.AddTransient<IWorkflowCategoryService, WorkflowCategoryService>();
                services.AddTransient<IWorkflowService, WorkflowService>();
                services.AddTransient<IWorkingTimeService, WorkingTimeService>();
                services.AddTransient<IWorkweekService, WorkweekService>();
                services.AddTransient<IWorkflowStepService, WorkflowStepService>();
                services.AddTransient<ISupportContactService, SupportContactService>();
            }
        }

        public static void ConfigureHelperServices(this IServiceCollection services, IConfiguration configuration)
        {
            if (services != null)
            {
                services.AddSingleton<ICryptoEncryptionHelper, CryptoEncryptionHelper>();
                services.AddScoped<IHttpContextHelper, HttpContextHelper>();
            }
        }

        public static void ConfigureRepositoryServices(this IServiceCollection services)
        {
            if (services != null)
            {
            }
        }

        public static void ConfigureADFSServices(this IServiceCollection services, IConfiguration configuration)
        {
            if (services != null)
            {
                var Wtrealm = configuration.GetValue<string>("ADFS:Wtrealm");
                var MetadataAddress = configuration.GetValue<string>("ADFS:MetadataAddress");
                services.AddAuthentication(sharedOptions =>
                {
                    sharedOptions.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    sharedOptions.DefaultChallengeScheme = WsFederationDefaults.AuthenticationScheme;
                    //sharedOptions.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                }).AddWsFederation(options =>
                {
                    options.Wtrealm = Wtrealm;
                    options.MetadataAddress = MetadataAddress;

                }).AddCookie();

                services.AddHttpContextAccessor();
            }
        }

        public static void ConfigureMappings(this IServiceCollection services)
        {
            if (services != null)
            {
                //Automap settings
                services.AddAutoMapper(Assembly.GetExecutingAssembly());
            }
        }

        public static void ConfigureDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            if (services != null)
            {
                //DbContext settings
                services.AddControllersWithViews();
                services.AddDbContext<WorkFlowDbContext>(options =>
                       options.UseSqlServer(configuration.GetConnectionString("WorkFlowConnection")), ServiceLifetime.Transient);

                //services.AddIdentity<IdentityUser, IdentityRole>(options =>
                //{
                //    options.Password.RequiredLength = 1;
                //    options.Password.RequireLowercase = false;
                //    options.Password.RequireUppercase = false;
                //    options.Password.RequireNonAlphanumeric = false;
                //    options.Password.RequireDigit = false;
                //})
                //.AddUserStore<WorkFlowDbContext>()
                //.AddDefaultTokenProviders();
            }
        }
    }
}
