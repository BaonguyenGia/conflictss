﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V2
{
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/users")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IPersonalProfileService _personalProfileService;
        private readonly IMapper _mapper;

        public UserController(IPersonalProfileService personalProfileService)
        {
            _personalProfileService = personalProfileService;
        }

        [HttpPut]
        [Route("UpdatePersonalProfile")]
        public async Task<IActionResult> UpdatePersonalProfile(int id, [FromQuery] string secretKey)
        {
            if (secretKey == "18101995")
            {
                await _personalProfileService.SecretUpdatePersonalProfile();
                return Ok();
            }
            else
            {
                return BadRequest("Your secret key is not valid");
            }
        }
    }
}
