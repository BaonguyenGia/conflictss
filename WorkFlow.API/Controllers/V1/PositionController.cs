﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/positions")]//required for default versioning
    [Route("api/v{version:apiVersion}/positions")]
    [ApiController]
    public class PositionController : Controller
    {
        private IPositionService _positionService;
        public PositionController(IPositionService positionService)
        {
            _positionService = positionService;
        }

        // GET: api/positions
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _positionService.GetPositions(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/positions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _positionService.GetPosition(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/positions
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] PositionResource positionResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _positionService.CreatePosition(positionResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/positions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] PositionResource positionResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _positionService.UpdatePosition(id, positionResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/positions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _positionService.DeletePosition(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
