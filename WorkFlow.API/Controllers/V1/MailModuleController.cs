﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Services.Contracts;

namespace WorkFlow.API.Controllers.V1
{
    [ApiVersion("1.0")]
    [Route("api/mailModules")]//required for default versioning
    [Route("api/v{version:apiVersion}/mailModules")]
    [ApiController]
    public class MailModuleController : Controller
    {
        private IMailModuleService _mailModuleService;

        public MailModuleController(IMailModuleService mailModuleService)
        {
            _mailModuleService = mailModuleService;
        }

        // GET: api/mailModules
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] QueryResource queryResource)
        {
            var apiResponse = await _mailModuleService.GetMailModules(queryResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // GET api/mailModules/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            var apiResponse = await _mailModuleService.GetMailModule(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // POST api/mailModules
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] MailModuleResource mailModuleResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _mailModuleService.CreateMailModule(mailModuleResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // PUT api/mailModules/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(long id, [FromBody] MailModuleResource mailModuleResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var apiResponse = await _mailModuleService.UpdateMailModule(id, mailModuleResource);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }

        // DELETE api/mailModules/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            var apiResponse = await _mailModuleService.DeleteMailModule(id);
            return apiResponse.IsError ? BadRequest(apiResponse.Message) : Ok(apiResponse.Data);
        }
    }
}
