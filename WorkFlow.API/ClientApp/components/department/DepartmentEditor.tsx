import * as React from "react";
import { IDepartmentModel } from "@Models/IDepartmentModel";
import { Formik, Field } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import TreeView from "@Components/shared/TreeView";
import { IDepartmentTreeViewModel } from "@Models/IDepartmentTreeViewModel";
import { MAX_LENGTH_INPUT } from "@Constants";
import { Typeahead } from "react-bootstrap-typeahead";
import { Button, Modal } from "react-bootstrap";
import { Search, XCircle, Check2Circle } from "react-bootstrap-icons";

export interface IProps {
  data: IDepartmentModel;
  isEditted: boolean | null;
  parentOptions: IDepartmentModel[];
  departmentTreeViews: IDepartmentTreeViewModel[];
  onSubmit: (data: IDepartmentModel) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const DepartmentEditor: React.FC<IProps> = (props: IProps) => {
  const formValidator = React.useRef<FormValidator>(null);
  const [isFindParentModalOpen, setIsFindParentModalOpen] =
    React.useState(false);
  const [selectedId, setSelectedId] = React.useState<number | null>(null);
  const [isPhoneNumberError, setPhoneNumberError] =
    React.useState<boolean>(false);
  const [selected, setselected] = React.useState<IDepartmentModel[]>([]);
  React.useEffect(() => {
    var parent = props.parentOptions.find(
      (p) => p.id === props.data?.parentId ?? 0
    );
    if (parent != undefined) {
      const defaultSelected = [parent];
      setselected(defaultSelected);
    }
  }, []);
  const onSubmitForm = (values: IDepartmentModel) => {
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values);
  };

  // This function will be passed to children components as a parameter.
  // It's necessary to build custom markup with controls outside this component.
  const renderEditor = (
    values: IDepartmentModel,
    setFieldValue: (
      field: string,
      value: any,
      shouldValidate?: boolean
    ) => void,
    defaultSelected: IDepartmentModel[],
    isEditted: boolean | null,
    parentOptions: IDepartmentModel[],
    departmentTreeViews: IDepartmentTreeViewModel[]
  ) => {
    return (
      <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.title)}>
            {({ field }) => (
              <>
                <label
                  className="control-label required"
                  htmlFor="department__title"
                >
                  Tiêu đề
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="department__title"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền tiêu đề."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>

        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.code)}>
            {({ field }) => (
              <>
                <label
                  className="control-label required"
                  htmlFor="department__code"
                >
                  Mã đơn vị
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="department__code"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>

        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.parentId)}>
            {({ field }) => (
              <>
                <label className="control-label" htmlFor="department__parentId">
                  Đơn vị cha
                </label>
                <div className="type-head">
                  <Typeahead
                    paginationOption={null}
                    clearButton
                    id="department__parentId"
                    options={parentOptions}
                    placeholder="Chọn đơn vị cha..."
                    // clearButton
                    onChange={(s) => {
                      setFieldValue(
                        nameof.full<IDepartmentModel>((x) => x.parentId),
                        s.length > 0 ? s[0].id : 0
                      );
                      setselected(s);
                    }}
                    selected={selected}
                    defaultSelected={defaultSelected}
                  ></Typeahead>
                  <Button onClick={() => setIsFindParentModalOpen(true)}>
                    <Search />
                  </Button>
                </div>
              </>
            )}
          </Field>
        </div>

        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.order)}>
            {({ field }) => (
              <>
                <label className="control-label" htmlFor="department__order">
                  Thứ tự
                </label>
                <input
                  type="number"
                  className="form-control"
                  id="department__order"
                  name={field.name}
                  data-value-type="number"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.email)}>
            {({ field }) => (
              <>
                <label className="control-label" htmlFor="department__email">
                  Email
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="department__email"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  data-msg-email="Bạn đã nhập sai định dạng email"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>

        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.fax)}>
            {({ field }) => (
              <>
                <label className="control-label" htmlFor="department__fax">
                  Fax
                </label>
                <input
                  type="email"
                  className="form-control"
                  id="department__fax"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  data-msg-email="Bạn đã nhập sai định dạng fax"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IDepartmentModel>((x) => x.telephone)}>
            {({ field }) => (
              <>
                <label
                  className="control-label"
                  htmlFor="department__telephone"
                >
                  Telephone
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="department__telephone"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  date-msg-number="helo"
                  value={field.value || ""}
                  onChange={(s) => {
                    {
                      field.onChange(s);
                      checkValidation(s.target.value)
                        ? setPhoneNumberError(false)
                        : setPhoneNumberError(true);
                    }
                  }}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </>
            )}
          </Field>
          {isPhoneNumberError && (
            <div className="tippy-popper tippy-margin" id="tippy-popper-custom">
              <div className="tippy-tooltip dark-theme" id="tippy-custom">
                <div className="tippy-arrow" id="tippy-arrow-custom"></div>
                <div className="tippy-content" data-state="visible">
                  Số điện thoại không hợp lệ.
                </div>
              </div>
            </div>
          )}
        </div>

        {/* Find Parent modal */}
        <Modal
          show={isFindParentModalOpen}
          onHide={() => setIsFindParentModalOpen(false)}
          className="find-parent"
        >
          <Modal.Header closeButton>
            <Modal.Title>Tìm kiếm đơn vị</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <TreeView
              departmentTreeViews={departmentTreeViews}
              chooseDepartment={(id) => {
                setSelectedId(id);
              }}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => setIsFindParentModalOpen(false)}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button variant="primary" onClick={() => chooseDepartment()}>
              <Check2Circle /> Chọn
            </Button>
          </Modal.Footer>
        </Modal>
      </FormValidator>
    );
    function checkValidation(phone) {
      let result = true;
      console.log(phone);
      const regexPhone = /^[0-9()-.]+$/;
      if (phone != "") {
        if (!regexPhone.test(phone)) {
          result = false;
        }
      }
      return result;
    }

    function chooseDepartment() {
      setIsFindParentModalOpen(false);
      setFieldValue("parentId", selectedId);
      var parent = props.parentOptions.find((p) => p.id === selectedId ?? 0);
      if (parent != undefined) {
        const defaultSelected = [parent];

        setselected(defaultSelected);
      }
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        if (!isPhoneNumberError) {
          onSubmitForm(values);
        }
      }}
    >
      {({ values, setFieldValue, handleSubmit }) => {
        // Let's say that the children element is a parametrizable function.
        // So we will pass other elements to this functional component as children
        // elements of this one:
        // <PersonEditor>
        // {(renderEditor, handleSubmit) => <>
        //     {renderEditor()}
        //     <button onClick={x => handleSubmit()}>Submit</button>
        // </>}
        // </PersonEditor>.
        return props.children(
          () =>
            renderEditor(
              values,
              setFieldValue,
              selected,
              props.isEditted,
              props.parentOptions.filter((x) => x.id !== values?.id ?? 0),
              props.departmentTreeViews
            ),
          handleSubmit
        );
      }}
    </Formik>
  );
};

export default DepartmentEditor;
