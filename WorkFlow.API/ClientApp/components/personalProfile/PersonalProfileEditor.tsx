import * as React from "react";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import { Formik, Field } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import { removeTime } from "@Services/FormatDateTimeService";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";
import DateTimePicker from "@Components/shared/DateTimePicker";
import { Typeahead } from "react-bootstrap-typeahead";
import { Button, Modal } from "react-bootstrap";
import { IPositionModel } from "@Models/IPositionModel";
import { IDepartmentModel } from "@Models/IDepartmentModel";
import { IDepartmentTreeViewModel } from "@Models/IDepartmentTreeViewModel";
import { Search, XCircle, Check2Circle } from "react-bootstrap-icons";
import TreeView from "@Components/shared/TreeView";
import ImageUpload from "@Components/shared/ImageUpload";

export interface IProps {
  data: IPersonalProfileModel;
  isEditted: boolean | null;
  positionOptions: IPositionModel[];
  departmentOptions: IDepartmentModel[];
  departmentTreeViews: IDepartmentTreeViewModel[];
  personalProfileOptions: IPersonalProfileModel[];
  onSubmit: (data: IPersonalProfileModel, file: File | null) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const PersonalProfileEditor: React.FC<IProps> = (props: IProps) => {
  const [selectedDepartmentId, setSelectedDepartmentId] = React.useState<
    number | null
  >(null);
  const [avatar, setAvatar] = React.useState<File | null>(null);
  const [isFindDepartmentModalOpen, setIsFindDepartmentModalOpen] =
    React.useState(false);
  const formValidator = React.useRef<FormValidator>(null);
  const [positionSelected, setPositionSelected] = React.useState<
    IPositionModel[]
  >([]);
  const [departmentSelected, setDepartmentSelected] = React.useState<
    IDepartmentModel[]
  >([]);
  const [managerSelected, setManagerSelected] = React.useState<
    IPersonalProfileModel[]
  >([]);

  React.useEffect(() => {
    var position = props.positionOptions.find(
      (p) => p.id === props.data?.positionId ?? 0
    );
    if (position != undefined) {
      const defaultPositionSelected = [position];
      setPositionSelected(defaultPositionSelected);
    }

    var department = props.departmentOptions.find(
      (p) => p.id === props.data?.departmentId ?? 0
    );
    if (department != undefined) {
      const defaultDepartmentSelected = [department];
      setDepartmentSelected(defaultDepartmentSelected);
    }

    var manager = props.personalProfileOptions.find(
      (p) => p.id === props.data?.managerId ?? 0
    );
    if (manager != undefined) {
      const defaultManagerSelected = [manager];
      setManagerSelected(defaultManagerSelected);
    }
  }, []);
  const onSubmitForm = (values: IPersonalProfileModel) => {
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values, avatar);
  };

  // This function will be passed to children components as a parameter.
  // It's necessary to build custom markup with controls outside this component.
  const renderEditor = (
    values: IPersonalProfileModel,
    setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void
  ) => {
    return (
      <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
        <div className="form-group">
          <Field
            type="email"
            name={nameof.full<IPersonalProfileModel>((x) => x.email)}
          >
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label required"
                  htmlFor="personalProfile__email"
                >
                  Tài khoản
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="personalProfile__email"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền email tài khoản."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                  disabled={props.isEditted ? true : false}
                />
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.fullName)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__fullName"
                >
                  Họ tên
                </label>
                <input
                  className="form-control"
                  id="personalProfile__fullName"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.staffId)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__staffId"
                >
                  Mã nhân viên
                </label>
                <input
                  className="form-control"
                  id="personalProfile__staffId"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.birthDay)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__birthDay"
                >
                  Ngày sinh
                </label>
                <div>
                  <DateTimePicker
                    date={field.birthDay}
                    chooseDateTime={(date) => {
                      setFieldValue("birthDay", removeTime(date));
                    }}
                  />
                </div>
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.address)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__address"
                >
                  Địa chỉ
                </label>
                <textarea
                  className="form-control"
                  id="personalProfile__address"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_TEXTAREA}
                />
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.mobile)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__mobile"
                >
                  Điện thoại
                </label>
                <input
                  className="form-control"
                  id="personalProfile__mobile"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.dateOfHire)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__dateOfHire"
                >
                  Ngày vào làm
                </label>
                <div>
                  <DateTimePicker
                    date={field.dateOfHire}
                    chooseDateTime={(date) => {
                      setFieldValue("dateOfHire", removeTime(date));
                    }}
                  />
                </div>
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.positionId)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__positionId"
                >
                  Chức vụ
                </label>
                <div className="type-head">
                  <Typeahead
                    paginationOption={null}
                    clearButton
                    id="personalProfile__positionId"
                    options={props.positionOptions}
                    placeholder="Chọn chức vụ..."
                    labelKey="title"
                    onChange={(s) => {
                      setFieldValue(
                        nameof.full<IPersonalProfileModel>((x) => x.positionId),
                        s.length > 0 ? s[0].id : 0
                      );
                      setPositionSelected(s);
                    }}
                    selected={positionSelected}
                    defaultSelected={positionSelected}
                  ></Typeahead>
                </div>
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field
            name={nameof.full<IPersonalProfileModel>((x) => x.departmentId)}
          >
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__departmentId"
                >
                  Đơn vị
                </label>
                <div className="type-head">
                  <Typeahead
                    paginationOption={null}
                    clearButton
                    id="personalProfile__departmentId"
                    options={props.departmentOptions}
                    placeholder="Chọn đơn vị ..."
                    labelKey="title"
                    onChange={(s) => {
                      setFieldValue(
                        nameof.full<IPersonalProfileModel>(
                          (x) => x.departmentId
                        ),
                        s.length > 0 ? s[0].id : 0
                      );
                      setDepartmentSelected(s);
                    }}
                    selected={departmentSelected}
                    defaultSelected={departmentSelected}
                  ></Typeahead>
                  <Button onClick={() => setIsFindDepartmentModalOpen(true)}>
                    <Search />
                  </Button>
                </div>
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.managerId)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__managerId"
                >
                  Quản lý trực tiếp
                </label>
                <div className="type-head">
                  <Typeahead
                    paginationOption={null}
                    clearButton
                    id="personalProfile__managerId"
                    options={props.personalProfileOptions}
                    placeholder="Chọn quản lý ..."
                    // clearButton
                    onChange={(s) => {
                      setFieldValue(
                        nameof.full<IPersonalProfileModel>((x) => x.managerId),
                        s.length > 0 ? s[0].id : 0
                      );
                      setManagerSelected(s);
                    }}
                    labelKey="fullName"
                    selected={managerSelected}
                    defaultSelected={managerSelected}
                  ></Typeahead>
                  <Button onClick={() => {}}>
                    <Search />
                  </Button>
                </div>
              </div>
            )}
          </Field>
        </div>

        <div className="form-group">
          <Field name={nameof.full<IPersonalProfileModel>((x) => x.userStatus)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__managerId"
                >
                  Tình trạng
                </label>
                <div>
                  <select
                    onChange={(event) => {
                      setFieldValue(
                        nameof.full<IPersonalProfileModel>((x) => x.userStatus),
                        Number(event.target.value)
                      );
                    }}
                    value={field.value ? field.value.toString() : ""}
                  >
                    <option value="1" label="Chính thức" />
                    <option value="2" label="Thử việc" />
                    <option value="3" label="Sắp thôi việc" />
                    <option value="-1" label="Nghỉ việc" />
                  </select>
                </div>
              </div>
            )}
          </Field>
        </div>

        <div className="form-group">
          <Field>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="personalProfile__imagePath"
                >
                  Hình dại diện
                </label>
                <div>
                  <ImageUpload
                    defaultSrc={
                      values && values.imagePath && values.imagePath.length > 0
                        ? `/api/personalProfiles/${values.id}/getAvatarById`
                        : null
                    }
                    chooseFile={(file) => setAvatar(file)}
                  />
                </div>
              </div>
            )}
          </Field>
        </div>

        <div className="form-group-divide">
          <div className="form-group">
            <Field
              name={nameof.full<IPersonalProfileModel>(
                (x) => x.signatureUserName
              )}
            >
              {({ field }) => (
                <div className="custom-field">
                  <label
                    className="control-label"
                    htmlFor="personalProfile__signatureUserName"
                  >
                    Tài khoản ký số cá nhân
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="personalProfile__signatureUserName"
                    name={field.name}
                    data-value-type="string"
                    data-val-required="false"
                    value={field.value || ""}
                  />
                </div>
              )}
            </Field>
          </div>

          <div className="form-group">
            <Field
              name={nameof.full<IPersonalProfileModel>(
                (x) => x.signaturePassword
              )}
            >
              {({ field }) => (
                <div className="custom-field">
                  <label
                    className="control-label"
                    htmlFor="personalProfile__signaturePassword"
                  >
                    Mật khẩu ký số cá nhân
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="personalProfile__signaturePassword"
                    name={field.name}
                    data-value-type="string"
                    data-val-required="false"
                    value={field.value || ""}
                  />
                </div>
              )}
            </Field>
          </div>
        </div>

        <div className="form-group-divide">
          <div className="form-group">
            <Field
              name={nameof.full<IPersonalProfileModel>((x) => x.signatureTitle)}
            >
              {({ field }) => (
                <div className="custom-field">
                  <label
                    className="control-label"
                    htmlFor="personalProfile__signatureTitle"
                  >
                    Tiêu đề chữ ký
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="personalProfile__signatureTitle"
                    name={field.name}
                    data-value-type="string"
                    data-val-required="false"
                    value={field.value || ""}
                  />
                </div>
              )}
            </Field>
          </div>

          <div className="form-group">
            <Field
              name={nameof.full<IPersonalProfileModel>((x) => x.signatureEmail)}
            >
              {({ field }) => (
                <div className="custom-field">
                  <label
                    className="control-label"
                    htmlFor="personalProfile__signatureEmail"
                  >
                    Mật khẩu ký số cá nhân
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    id="personalProfile__signatureEmail"
                    name={field.name}
                    data-value-type="string"
                    data-val-required="false"
                    value={field.value || ""}
                  />
                </div>
              )}
            </Field>
          </div>
        </div>

        {/* Find department modal */}
        <Modal
          show={isFindDepartmentModalOpen}
          onHide={() => setIsFindDepartmentModalOpen(false)}
          className="find-parent"
        >
          <Modal.Header closeButton>
            <Modal.Title>Tìm kiếm đơn vị</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <TreeView
              departmentTreeViews={props.departmentTreeViews}
              chooseDepartment={(id) => {
                setSelectedDepartmentId(id);
              }}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => setIsFindDepartmentModalOpen(false)}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button variant="primary" onClick={() => chooseDepartment()}>
              <Check2Circle /> Chọn
            </Button>
          </Modal.Footer>
        </Modal>
      </FormValidator>
    );

    function chooseDepartment() {
      setIsFindDepartmentModalOpen(false);
      setFieldValue("departmentId", selectedDepartmentId);
      var department = props.departmentOptions.find(
        (p) => p.id === selectedDepartmentId ?? 0
      );
      if (department != undefined) {
        const defaultSelected = [department];

        setDepartmentSelected(defaultSelected);
      }
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        onSubmitForm(values);
      }}
    >
      {({ values, setFieldValue, handleSubmit }) => {
        // Let's say that the children element is a parametrizable function.
        // So we will pass other elements to this functional component as children
        // elements of this one:
        // <PersonEditor>
        // {(renderEditor, handleSubmit) => <>
        //     {renderEditor()}
        //     <button onClick={x => handleSubmit()}>Submit</button>
        // </>}
        // </PersonEditor>.
        return props.children(
          () => renderEditor(values, setFieldValue),
          handleSubmit
        );
      }}
    </Formik>
  );
};

export default PersonalProfileEditor;
