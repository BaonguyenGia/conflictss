﻿import * as React from "react";
import { ISupportContactModel } from "@Models/ISupportContactModel";
import { Formik, Field, ErrorMessage } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";
import { Typeahead } from "react-bootstrap-typeahead";
import { Button, Modal } from "react-bootstrap";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import { Search } from "react-bootstrap-icons";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";

export interface IProps {
  data: ISupportContactModel;
  isEditted: boolean | null;
  personalProfileOptions: IPersonalProfileModel[];
  onSubmit: (data: ISupportContactModel) => void;
  children: (
    renderEditor: () => JSX.Element,
    submit: () => void
  ) => JSX.Element;
}

const SupportContactEditor: React.FC<IProps> = (props: IProps) => {
  const formValidator = React.useRef<FormValidator>(null);
  const [personalProfileSelected, setpersonalProfileSelected] = React.useState<
    IPersonalProfileModel[]
  >([]);
  const [isPersonalIdError, setPersonalIdError] =
    React.useState<boolean>(false);
  const [isPhoneNumberError, setPhoneNumberError] =
    React.useState<boolean>(false);
  React.useEffect(() => {
    var personalProfile = props.personalProfileOptions.find(
      (p) => p.id === props.data?.personalProfileId ?? 0
    );
    if (personalProfile != undefined) {
      const defaultPersonalProfileSelected = [personalProfile];
      setpersonalProfileSelected(defaultPersonalProfileSelected);
    }
  }, []);
  const onSubmitForm = (values: ISupportContactModel) => {
    if (!formValidator.current.isValid()) {
      // Form is not valid.
      return;
    }
    props.onSubmit(values);
  };

    // This function will be passed to children components as a parameter.
    // It's necessary to build custom markup with controls outside this component.
    const renderEditor = (
        values: ISupportContactModel,
        isEditted: boolean | null,
        defaultSelected: IPersonalProfileModel[],
        setFieldValue: (field: string, value: any, shouldValidate?: boolean) => void
    ) => {
        return (
            <FormValidator
                className="form popupSupport"
                ref={(x) => (formValidator.current = x)}
                id="supportContactEditorForm"
            >
                <div className="form-group">
                    <Field name={nameof.full<ISupportContactModel>((x) => x.title)}>
                        {({ field }) => (
                            <div className="custom-field">
                                <label
                                    className="control-label required"
                                    htmlFor="suportContact__title"
                                >
                                    Tiêu đề
                </label>
                <input
                  type="text"
                  className="form-control"
                  id="supportContact__title"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="true"
                  data-msg-required="Vui lòng điền tiêu đề."
                  value={field.value || ""}
                  onChange={field.onChange}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </div>
            )}
          </Field>
        </div>
        <div className="form-group">
          <Field name={nameof.full<ISupportContactModel>((x) => x.phoneNumber)}>
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label"
                  htmlFor="supportContact__phoneNumber"
                >
                  Số điện thoại
                </label>
                <input
                  className="form-control"
                  id="supportContact__phoneNumber"
                  name={field.name}
                  data-value-type="string"
                  data-val-required="false"
                  value={field.value || ""}
                  onChange={(s) => {
                    {
                      setPhoneNumberError(false);
                      field.onChange(s);
                    }
                  }}
                  maxLength={MAX_LENGTH_INPUT}
                />
              </div>
            )}
          </Field>
        </div>
        {isPhoneNumberError && (
          <div className="tippy-popper" id="tippy-popper-custom">
            <div className="tippy-tooltip dark-theme" id="tippy-custom">
              <div className="tippy-arrow" id="tippy-arrow-custom"></div>
              <div className="tippy-content" data-state="visible">
                Số điện thoại không hợp lệ.
              </div>
            </div>
          </div>
        )}
        <div className="form-group">
          <Field
            name={nameof.full<ISupportContactModel>((x) => x.personalProfileId)}
          >
            {({ field }) => (
              <div className="custom-field">
                <label
                  className="control-label required"
                  htmlFor="supportContact__personalProfileID"
                >
                  Tài khoản
                </label>
                <div className="type-head">
                  <Typeahead
                    paginationOption={null}
                    clearButton
                    id="supportContact__personalProfileID"
                    options={props.personalProfileOptions}
                    placeholder="Chọn tài khoản ..."
                    onChange={(s) => {
                      {
                        setFieldValue(
                          nameof.full<ISupportContactModel>(
                            (x) => x.personalProfileId
                          ),
                          s.length > 0 ? s[0].id : 0
                        );
                        setPersonalIdError(false);
                        setpersonalProfileSelected(s);
                      }
                    }}
                    labelKey="fullName"
                    selected={personalProfileSelected}
                    defaultSelected={defaultSelected}
                  />
                  <Button>
                    <Search />
                  </Button>
                </div>
              </div>
            )}
          </Field>
        </div>
        {isPersonalIdError && (
          <div className="tippy-popper" id="tippy-popper-custom">
            <div className="tippy-tooltip dark-theme" id="tippy-custom">
              <div className="tippy-arrow" id="tippy-arrow-custom"></div>
              <div className="tippy-content" data-state="visible">
                Vui lòng chọn tài khoản.
              </div>
            </div>
          </div>
        )}
        {isEditted && (
          <div>
            <div className="form-group-divide">
              <div className="form-group">
                <Field
                  name={nameof.full<ISupportContactModel>((x) => x.createdBy)}
                >
                  {({ field }) => (
                    <>
                      <label
                        className="control-label"
                        htmlFor="supportContact__createdBy"
                      >
                        Người tạo
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="supportContact__createdBy"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={field.value || ""}
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>

              <div className="form-group">
                <Field
                  name={nameof.full<ISupportContactModel>((x) => x.createdTime)}
                >
                  {({ field }) => (
                    <>
                      <label className="control-label" htmlFor="createdTime">
                        Vào lúc
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="supportContact__createdTime"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={
                          field.value ? formatTimeInEntity(field.value) : ""
                        }
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>
            </div>
            <div className="form-group-divide">
              <div className="form-group">
                <Field
                  name={nameof.full<ISupportContactModel>((x) => x.modifiedBy)}
                >
                  {({ field }) => (
                    <>
                      <label
                        className="control-label"
                        htmlFor="supportContact__createdBy"
                      >
                        Người chỉnh sửa
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="supportContact__modifieddBy"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={field.value || ""}
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>
              <div className="form-group">
                <Field
                  name={nameof.full<ISupportContactModel>(
                    (x) => x.lastModified
                  )}
                >
                  {({ field }) => (
                    <>
                      <label className="control-label" htmlFor="createdTime">
                        Vào lúc
                      </label>
                      <input
                        type="text"
                        className="form-control"
                        id="supportContact__lastModified"
                        name={field.name}
                        data-value-type="string"
                        data-val-required="false"
                        value={
                          field.value ? formatTimeInEntity(field.value) : ""
                        }
                        disabled={true}
                      />
                    </>
                  )}
                </Field>
              </div>
            </div>
          </div>
        )}
      </FormValidator>
    );
  };

  function checkValidation(values) {
    let result = true;
    const regexPhone = /^[0-9()-.]+$/;
    if (!values.personalProfileId || values.personalProfileId == 0) {
      setPersonalIdError(true);
      result = false;
    }
    if (values.phoneNumber != "" && values.phoneNumber != undefined) {
      if (!regexPhone.test(values.phoneNumber)) {
        setPhoneNumberError(true);
        result = false;
      }
    }
    return result;
  }

  return (
    <Formik
      enableReinitialize
      initialValues={props.data}
      onSubmit={(values, { setSubmitting }) => {
        if (checkValidation(values)) {
          onSubmitForm(values);
        }
      }}
    >
      {({ values, setFieldValue, handleSubmit }) => {
        return props.children(
          () =>
            renderEditor(
              values,
              props.isEditted,
              personalProfileSelected,
              setFieldValue
            ),
          handleSubmit
        );
      }}
    </Formik>
  );
};

export default SupportContactEditor;
