import React, { useState, useEffect } from "react";
import DateTimePicker from "@Components/shared/DateTimePicker";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import { IWorkflowModel } from "@Models/IWorkflowModel";
import { Typeahead } from "react-bootstrap-typeahead";
import { IWorkflowCategoryModel } from "@Models/IWorkflowCategoryModel";
import ImageUpload from "@Components/shared/ImageUpload";

export interface IProps {
  personalProfiles: IPersonalProfileModel[];
  workflowCategories: IWorkflowCategoryModel[];
  updateMainData: (data: IWorkflowModel) => void;
  workflowParent: IWorkflowModel;
}

const Information: React.FC<IProps> = (props: IProps) => {
  const {
    personalProfiles,
    workflowCategories,
    workflowParent,
    updateMainData,
  } = props;

  const [createPermission, setCreatePermission] = useState(
    getDefaultCreatePermission(workflowParent, personalProfiles)
  );
  const [seenPermission, setSeenPermission] = useState(
    getDefaultSeenPermission(workflowParent, personalProfiles)
  );
  const [workflowCategory, setWorkflowCategory] = useState<
    IWorkflowCategoryModel[]
  >(getDefaultWorkflowCategory(workflowParent, workflowCategories));
  const [workflow, setWorkflow] = useState<IWorkflowModel>(workflowParent);

  useEffect(() => {
    setWorkflow({
      ...workflow,
      categoryId: workflowCategory.length === 0 ? null : workflowCategory[0].id,
    });
  }, [workflowCategory]);

  useEffect(() => {
    console.log(workflow);
    updateMainData(workflow);
  }, [workflow]);

  useEffect(() => {
    var newSeenPermission = [];
    seenPermission.forEach((element) => {
      newSeenPermission.push(element.id);
    });

    setWorkflow({ ...workflow, seenPermission: newSeenPermission.join() });
  }, [seenPermission]);

  useEffect(() => {
    var newCreatePermission = [];
    createPermission.forEach((element) => {
      newCreatePermission.push(element.id);
    });
    console.log(newCreatePermission);
    console.log({ ...workflow, createPermission: newCreatePermission.join() });
    setWorkflow({ ...workflow, createPermission: newCreatePermission.join() });
  }, [createPermission]);

  return (
    <div>
      <span className="info-title">Information</span>
      <div>
        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Tiêu đề</span>
            <label>(*)</label>
          </div>
          <div className="ItemInput">
            <input type="text" name="title" onChange={handleChange} />
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Ngày bắt đầu</span>
          </div>
          <div className="ItemInput">
            <DateTimePicker date={null} chooseDateTime={() => {}} />
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Ngày kết thúc</span>
          </div>
          <div className="ItemInput">
            <DateTimePicker date={null} chooseDateTime={() => {}} />
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Quyền tạo</span>
          </div>
          <div className="ItemInput">
            <div className="type-head-multiple">
              {personalProfiles && (
                <Typeahead
                  id={"createPermission"}
                  paginationOption={null}
                  clearButton
                  options={personalProfiles}
                  placeholder="Enter user or Group"
                  // clearButton
                  onChange={(s) => {
                    setCreatePermission(s);
                  }}
                  multiple
                  labelKey="fullName"
                  selected={createPermission}
                  defaultSelected={createPermission}
                ></Typeahead>
              )}
            </div>
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Quyền xem</span>
          </div>
          <div className="ItemInput">
            <div className="type-head-multiple">
              {personalProfiles && (
                <Typeahead
                  id={"seenPermission"}
                  paginationOption={null}
                  clearButton
                  options={personalProfiles}
                  placeholder="Enter user or Group"
                  // clearButton
                  onChange={(s) => {
                    setSeenPermission(s);
                  }}
                  multiple
                  labelKey="fullName"
                  selected={seenPermission}
                  defaultSelected={seenPermission}
                ></Typeahead>
              )}
            </div>
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Mã</span>
          </div>
          <div className="ItemInput">
            <input type="text" name="code" onChange={handleChange} />
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span> Chỉ số bắt đầu </span>
          </div>
          <div className="ItemInput">
            <input type="text" name="order" onChange={handleChange} />
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Category</span>
          </div>
          <div className="ItemInput">
            <div className="type-head">
              {personalProfiles && (
                <Typeahead
                  id={"workflowCategory"}
                  paginationOption={null}
                  clearButton
                  options={workflowCategories}
                  placeholder="Chọn category"
                  // clearButton
                  onChange={(s) => {
                    setWorkflowCategory(s);
                  }}
                  labelKey="title"
                  selected={workflowCategory}
                  defaultSelected={workflowCategory}
                  style={{ width: "100%" }}
                ></Typeahead>
              )}
            </div>
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Phiên bản quy trình</span>
          </div>
          <div className="ItemInput">
            <input type="text" name="version" onChange={handleChange} />
          </div>
        </div>

        <div className="ItemRowWorkflow">
          <div className="ItemText">
            <span>Hình ảnh</span>
          </div>
          <div className="ItemInput">
            <ImageUpload defaultSrc={null} chooseFile={(file) => {}} />
          </div>
        </div>
      </div>
    </div>
  );

  function handleChange(e: any) {
    const { name, value } = e.target;

    setWorkflow({ ...workflow, [name]: value });
  }
};

function getDefaultCreatePermission(
  workflowParent: IWorkflowModel,
  personalProfiles: IPersonalProfileModel[]
): any {
  if (
    workflowParent.createPermission &&
    workflowParent.createPermission.length > 0
  ) {
    var createPermissions = workflowParent.createPermission
      .split(",")
      .map((x) => +x);
    return personalProfiles.filter(
      (personalProfile) => createPermissions.indexOf(personalProfile.id) > -1
    );
  } else {
    return [];
  }
}

function getDefaultSeenPermission(
  workflowParent: IWorkflowModel,
  personalProfiles: IPersonalProfileModel[]
): any {
  if (
    workflowParent.seenPermission &&
    workflowParent.seenPermission.length > 0
  ) {
    var seenPermissions = workflowParent.seenPermission
      .split(",")
      .map((x) => +x);
    return personalProfiles.filter(
      (personalProfile) => seenPermissions.indexOf(personalProfile.id) > -1
    );
  } else {
    return [];
  }
}

function getDefaultWorkflowCategory(
  workflowParent: IWorkflowModel,
  workflowCategories: IWorkflowCategoryModel[]
): any {
  if (workflowParent.categoryId && workflowParent.categoryId > 0) {
    return workflowCategories.filter(
      (getDefaultWorkflowCategory) =>
        workflowParent.categoryId == getDefaultWorkflowCategory.id
    );
  } else {
    return [];
  }
}

export default Information;
