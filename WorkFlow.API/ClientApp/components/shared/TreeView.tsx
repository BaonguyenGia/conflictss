import React, { useState } from "react";
import Tree from "@naisutech/react-tree";
import { IDepartmentTreeViewModel } from "@Models/IDepartmentTreeViewModel";

export interface IProps {
  departmentTreeViews: IDepartmentTreeViewModel[];
  chooseDepartment: (id: number) => void;
}

const TreeView: React.FC<IProps> = (props: IProps) => {
  const myTheme = {
    "my-theme": {
      //   text: "#fff",
      //   bg: "white",
      highlight: "lightgray", // the colours used for selected and hover items
      //   decal: "hotpink", // the colours used  for open folder indicators and icons
      //   accent: "#999", // the colour used for row borders and empty file indicators
    },
  };

  const [theme, setTheme] = useState(myTheme);

  return (
    <Tree
      nodes={props.departmentTreeViews}
      customTheme={theme}
      theme={"my-theme"}
      grow
      onSelect={(e) => {
        props.chooseDepartment(e.id);
      }}
    />
  );
};

export default TreeView;
