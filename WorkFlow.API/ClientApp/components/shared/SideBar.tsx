import React from "react";
import { withRouter } from "react-router";
import { Nav, Accordion, Card } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import {
  ArrowRight,
  BagFill,
  PeopleFill,
  PersonFill,
  PersonLinesFill,
  List,
  PersonCircle,
  EnvelopeFill,
  UiChecks,
  Diagram3,
  CaretDownFill,
} from "react-bootstrap-icons";
import workflow from "@Images/workflow.png";

const SideBar: React.FC = () => {
  return (
    <Nav
      className=" d-none d-md-block  sidebar navBlue"
      activeKey="/home"
      onSelect={(selectedKey) => {}}
    >
      <Accordion defaultActiveKey="0">
        <Card className="navBlue">
          <Accordion.Toggle
            className=" navDarkBlue white pointer"
            as={Card.Header}
            eventKey="0"
          >
            Thiết kế quy trình
            <CaretDownFill className="iconMenuLoad" />
          </Accordion.Toggle>
          <Accordion.Collapse eventKey="0" className="navDarkBlue">
            <Card.Body>
              <LinkContainer exact to={"/workflows"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <i className="logoItem">
                    <img src={workflow} />
                  </i>
                  Quy trình động
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <Diagram3 className="logoItem" />
                  Quy trình chi tiết
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/mailtemplates"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white" >
                  <EnvelopeFill className="logoItem" />
                  Biểu mẫu mail
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <UiChecks className="logoItem" />
                  Kiểm tra Việc cần xử lý
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/documentTypes"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <i className="logoItem">
                    <img src={workflow} />
                  </i>
                  Định nghĩa loại tài liệu
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <i className="logoItem">
                    <img src={workflow} />
                  </i>
                  Cấu hình by Pass
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/workflowCategory"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <i className="logoItem">
                    <img src={workflow} />
                  </i>
                  Cấu hình loại quy trình
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/workingtimes"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <i className="logoItem">
                    <img src={workflow} />
                  </i>
                  Cấu hình thời gian làm việc
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/workweek"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <i className="logoItem">
                    <img src={workflow} />
                  </i>
                  Cấu hình tuần làm việc
                </Nav.Link>
              </LinkContainer>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
        <Card className="navDarkBlue">
          <Accordion.Toggle
            className="white pointer"
            as={Card.Header}
            eventKey="1"
          >
            Danh mục
            <CaretDownFill className="iconMenuLoad" />
          </Accordion.Toggle>

          <Accordion.Collapse eventKey="1">
            <Card.Body>
              <LinkContainer exact to={"/department"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <PeopleFill className="logoItem" />
                  Đơn vị
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/personalProfile"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <PersonFill className="logoItem" />
                  Nhân viên
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/position"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <PersonLinesFill className="logoItem" />
                  Chức vụ
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <List className="logoItem" />
                  Menu
                </Nav.Link>
              </LinkContainer>
              <LinkContainer exact to={"/supportContacts"} activeClassName="activeLink">
                <Nav.Link className="hoverNav white">
                  <PersonCircle className="logoItem" />
                  Support
                </Nav.Link>
              </LinkContainer>
            </Card.Body>
          </Accordion.Collapse>
        </Card>
      </Accordion>
    </Nav>
  );
};


// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(SideBar);
