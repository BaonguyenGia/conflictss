import React, { useState } from "react";
import NoImage from "@Images/svg/No_image_available.svg";

export interface IProps {
  defaultSrc: string;
  chooseFile: (file: File) => void;
}

const ImageUpload: React.FC<IProps> = (props: IProps) => {
  const [imagePreview, setImagePreview] = useState(
    props.defaultSrc && props.defaultSrc.length > 0 ? (
      <img
        id="avatar"
        src={props.defaultSrc}
        onError={() => _setErrorImage()}
      />
    ) : (
      <div className="previewText">Please select an Image for Preview</div>
    )
  );

  return (
    <div className="previewComponent">
      <div>
        <input
          className="fileInput"
          type="file"
          onChange={(e) => _handleImageChange(e)}
        />
      </div>

      <div className="imgPreview">{imagePreview}</div>
    </div>
  );

  function _handleImageChange(e) {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      props.chooseFile(file);
      if (reader.result) {
        setImagePreview(
          <img
            id="avatar"
            src={reader.result.toString()}
            onError={() => _setErrorImage()}
          />
        );
      } else {
        setImagePreview(
          <div className="previewText">Please select an Image for Preview</div>
        );
      }
    };

    reader.readAsDataURL(file);
  }

  function _setErrorImage() {
    var avatarImg = document.getElementById("avatar");

    avatarImg.setAttribute("src", NoImage);
  }
};

export default ImageUpload;
