﻿import * as React from "react";
import { IDocumentTypeModel } from "@Models/IDocumentTypeModel";
import { Formik, Field } from "formik";
import FormValidator from "@Components/shared/FormValidator";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";
import { MAX_LENGTH_INPUT, MAX_LENGTH_TEXTAREA } from "@Constants";

export interface IProps {
    data: IDocumentTypeModel;
    isEditted: boolean | null;
    onSubmit: (data: IDocumentTypeModel) => void;
    children: (
        renderEditor: () => JSX.Element,
        submit: () => void
    ) => JSX.Element;
}

const DocumentTypeEditor: React.FC<IProps> = (props: IProps) => {
    const formValidator = React.useRef<FormValidator>(null);

    const onSubmitForm = (values: IDocumentTypeModel) => {
        if (!formValidator.current.isValid()) {
            // Form is not valid.
            return;
        }
        props.onSubmit(values);
    };

    // This function will be passed to children components as a parameter.
    // It's necessary to build custom markup with controls outside this component.
    const renderEditor = (values: IDocumentTypeModel, isEditted: boolean | null) => {
        return (
            <FormValidator className="form" ref={(x) => (formValidator.current = x)}>
                <div className="form-group">
                    <Field name={nameof.full<IDocumentTypeModel>((x) => x.title)}>
                        {({ field }) => (
                            <>
                                <label
                                    className="control-label required"
                                    htmlFor="documentType__firstName"
                                >
                                    Tiêu đề
                </label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="documentType__title"
                                    name={field.name}
                                    data-value-type="string"
                                    data-val-required="true"
                                    data-msg-required="Vui lòng điền tiêu đề."
                                    value={field.value || ""}
                                    onChange={field.onChange}
                                    maxLength={MAX_LENGTH_INPUT}
                                />
                            </>
                        )}
                    </Field>
                </div>
                <div className="form-group">
                    <Field name={nameof.full<IDocumentTypeModel>((x) => x.description)}>
                        {({ field }) => (
                            <>
                                <label
                                    className="control-label"
                                    htmlFor="documentType__description"
                                >
                                    Mô tả
                </label>
                                <textarea
                                    className="form-control"
                                    id="documentType__description"
                                    name={field.name}
                                    data-value-type="string"
                                    data-val-required="false"
                                    value={field.value || ""}
                                    onChange={field.onChange}
                                    maxLength={MAX_LENGTH_TEXTAREA}
                                />
                            </>
                        )}
                    </Field>
                </div>

                {isEditted && (
                    <div>
                        <div className="form-group-divide">
                            <div className="form-group">
                                <Field name={nameof.full<IDocumentTypeModel>((x) => x.createdBy)}>
                                    {({ field }) => (
                                        <>
                                            <label
                                                className="control-label"
                                                htmlFor="documentType__createdBy"
                                            >
                                                Người tạo
                      </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="ocumentType__createdBy"
                                                name={field.name}
                                                data-value-type="string"
                                                data-val-required="false"
                                                value={field.value || ""}
                                                disabled={true}
                                            />
                                        </>
                                    )}
                                </Field>
                            </div>

                            <div className="form-group">
                                <Field name={nameof.full<IDocumentTypeModel>((x) => x.createdTime)}>
                                    {({ field }) => (
                                        <>
                                            <label className="control-label" htmlFor="createdTime">
                                                Vào lúc
                      </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="documentType__createdTime"
                                                name={field.name}
                                                data-value-type="string"
                                                data-val-required="false"
                                                value={
                                                    field.value ? formatTimeInEntity(field.value) : ""
                                                }
                                                disabled={true}
                                            />
                                        </>
                                    )}
                                </Field>
                            </div>
                        </div>

                        <div className="form-group-divide">
                            <div className="form-group">
                                <Field name={nameof.full<IDocumentTypeModel>((x) => x.modifiedBy)}>
                                    {({ field }) => (
                                        <>
                                            <label
                                                className="control-label"
                                                htmlFor="documentType__modifiedBy"
                                            >
                                                Người chỉnh sửa
                      </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="documentType__modifiedBy"
                                                name={field.name}
                                                data-value-type="string"
                                                data-val-required="false"
                                                value={field.value || ""}
                                                disabled={true}
                                            />
                                        </>
                                    )}
                                </Field>
                            </div>

                            <div className="form-group">
                                <Field
                                    name={nameof.full<IDocumentTypeModel>((x) => x.lastModified)}
                                >
                                    {({ field }) => (
                                        <>
                                            <label className="control-label" htmlFor="lastModified">
                                                Vào lúc
                      </label>
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="documentType__lastModified"
                                                name={field.name}
                                                data-value-type="string"
                                                data-val-required="false"
                                                value={
                                                    field.value ? formatTimeInEntity(field.value) : ""
                                                }
                                                disabled={true}
                                            />
                                        </>
                                    )}
                                </Field>
                            </div>
                        </div>
                    </div>
                )}
            </FormValidator>
        );
    };

    return (
        <Formik
            enableReinitialize
            initialValues={props.data}
            onSubmit={(values, { setSubmitting }) => {
                onSubmitForm(values);
            }}
        >
            {({ values, handleSubmit }) => {
                // Let's say that the children element is a parametrizable function.
                // So we will pass other elements to this functional component as children
                // elements of this one:
                // <PersonEditor>
                // {(renderEditor, handleSubmit) => <>
                //     {renderEditor()}
                //     <button onClick={x => handleSubmit()}>Submit</button>
                // </>}
                // </PersonEditor>.
                return props.children(
                    () => renderEditor(values, props.isEditted),
                    handleSubmit
                );
            }}
        </Formik>
    );
};

export default DocumentTypeEditor;
