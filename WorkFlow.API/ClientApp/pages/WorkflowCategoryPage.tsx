import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { IWorkflowCategoryModel } from "@Models/IWorkflowCategoryModel";
import * as WorkflowCategoryStore from "@Store/workflowCategoryStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import WorkflowCategoryEditor from "@Components/workflowCategory/WorkflowCategoryEditor";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import {
  Modal,
  Button,
  Container,
  Row,
  Card,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import Result from "@Core/Result";
import {
  Plus,
  Save,
  XCircle,
  Trash,
  PlusSquare,
  X,
  ArrowClockwise,
  ThreeDots,
} from "react-bootstrap-icons";

type Props = typeof WorkflowCategoryStore.actionCreators &
  WorkflowCategoryStore.IWorkflowCategoryStoreState &
  RouteComponentProps<{}>;

interface IState {
  searchTerm: string;
  currentPageNum: number;
  limitPerPage: number;
  isAddModalOpen: boolean;
  isUpdateModalOpen: boolean;
  isDeleteModalOpen: boolean;
  modelForEdit?: IWorkflowCategoryModel;
}

class WorkflowCategoryPage extends React.Component<Props, IState> {
  private paginator: Paginator;

  private debouncedSearch: (term: string) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchTerm: "",
      currentPageNum: 1,
      limitPerPage: 14,
      modelForEdit: null,
      isAddModalOpen: false,
      isDeleteModalOpen: false,
      isUpdateModalOpen: false,
    };

    // "AwesomeDebouncePromise" makes a delay between
    // the end of input term and search request.
    this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
      props.search(term);
    }, 500);

    this.props.search();
  }

  private refresh = () => {
    this.setState({ currentPageNum: 1 });
    wait(async () => {
      await this.props.search(this.state.searchTerm);
    }, "WorkflowCategoryPageTask");
  };

  private toggleAddWorkflowCategoryModal = () => {
    this.setState((prev) => ({
      isAddModalOpen: !prev.isAddModalOpen,
    }));
  };

  private toggleModelForEdit = (modelForEdit?: IWorkflowCategoryModel) => {
    this.setState((prev) => ({
      modelForEdit,
    }));
  };

  private toggleUpdateWorkflowCategoryModal = (
    modelForEdit?: IWorkflowCategoryModel
  ) => {
    this.setState((prev) => ({
      modelForEdit,
      isUpdateModalOpen: !prev.isUpdateModalOpen,
    }));
  };

  private toggleDeleteWorkflowCategoryModal = () => {
    const { modelForEdit } = this.state;
    if (modelForEdit == null) {
      return;
    }
    this.setState((prev) => ({
      modelForEdit: prev.isDeleteModalOpen === true ? null : prev.modelForEdit,
      isDeleteModalOpen: !prev.isDeleteModalOpen,
    }));
  };

  private addWorkflowCategory = async (data: IWorkflowCategoryModel) => {
    const { add } = this.props;
    var result = (await add(data)) as any as Result<IWorkflowCategoryModel>;

    if (!result.hasErrors) {
      this.paginator.setLastPage();
      this.toggleAddWorkflowCategoryModal();
    }
  };

  private updateWorkflowCategory = async (data: IWorkflowCategoryModel) => {
    const { update } = this.props;
    var result = await getPromiseFromActionCreator(update(data));

    if (!result.hasErrors) {
      this.toggleUpdateWorkflowCategoryModal();
    }
  };

  private deleteWorkflowCategory = async () => {
    const { remove } = this.props;
    const { modelForEdit } = this.state;

    await remove(modelForEdit.id);
    this.toggleDeleteWorkflowCategoryModal();
  };

  private renderRows = (data: IWorkflowCategoryModel[]) => {
    const { currentPageNum, limitPerPage, modelForEdit } = this.state;

    return paginate(data, currentPageNum, limitPerPage).map((item) => (
      <tr
        key={item.id}
        className={`subrow
        ${modelForEdit && item.id === modelForEdit.id ? " tr-selected" : ""}`}
        onClick={() => this.toggleModelForEdit(item)}
      >
        <td>
          <a
            className="subtitle"
            href="javascript:void(0)"
            onClick={(x) => this.toggleUpdateWorkflowCategoryModal(item)}
          >
            {item.title}
          </a>
        </td>
        <td>
          {item.lastModified
            ? formatTimeInEntity(item.lastModified)
            : formatTimeInEntity(item.createdTime)}
        </td>
      </tr>
    ));
  };

  private onClearSearchInput = () => {
    var val = "";
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    var val = e.currentTarget.value;
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  render() {
    const { items, isFetching } = this.props;
    const {
      isAddModalOpen,
      isUpdateModalOpen,
      modelForEdit,
      isDeleteModalOpen,
      limitPerPage,
      currentPageNum,
      searchTerm,
    } = this.state;

    return (
      <Container>
        <Helmet>
          <title>Cấu hình loại quy trình - All Items</title>
        </Helmet>

        <Card body className="mt-4 mb-4">
          <Row>
            <div className="col-8 col-sm-9 col-md-9 col-lg-8">
              <InputGroup className="mb-3">
                <FormControl
                  placeholder={"Tìm kiếm theo tiêu đề..."}
                  defaultValue={""}
                  value={searchTerm}
                  onChange={this.onChangeSearchInput}
                />
                {searchTerm.trim().length > 0 && (
                  <Button variant="secondary" onClick={this.onClearSearchInput}>
                    <X />
                  </Button>
                )}
              </InputGroup>
            </div>
            <div className="col-4 col-sm-3 col-md-3 col-lg-4">
              <Button
                className="button-reload"
                disabled={isFetching}
                onClick={(x) => this.refresh()}
              >
                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
              </Button>
            </div>
          </Row>
        </Card>

        <div className="option-panel">
          <div
            onClick={() => this.toggleAddWorkflowCategoryModal()}
            className="add"
          >
            <Plus size={30} color="green" /> Tạo mới
          </div>
          <div onClick={() => this.toggleDeleteWorkflowCategoryModal()}>
            <Trash size={20} color="red" /> Xóa
          </div>
        </div>

        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Đã sửa đổi</th>
            </tr>
          </thead>
          <tbody>{this.renderRows(items)}</tbody>
        </table>

        {/* Add modal */}
        <Modal
          show={isAddModalOpen}
          onHide={() => this.toggleAddWorkflowCategoryModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>Thêm mới</Modal.Title>
          </Modal.Header>

          <WorkflowCategoryEditor
            data={{} as IWorkflowCategoryModel}
            onSubmit={this.addWorkflowCategory}
            isEditted={false}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleAddWorkflowCategoryModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="success" onClick={(x) => handleSubmit()}>
                    <PlusSquare /> Thêm
                  </Button>
                </Modal.Footer>
              </>
            )}
          </WorkflowCategoryEditor>
        </Modal>

        {/* Update modal */}
        <Modal
          show={isUpdateModalOpen}
          onHide={() => this.toggleUpdateWorkflowCategoryModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Chỉnh sửa loại quy trình:{" "}
              {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>

          <WorkflowCategoryEditor
            data={modelForEdit}
            onSubmit={this.updateWorkflowCategory}
            isEditted={true}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleUpdateWorkflowCategoryModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="primary" onClick={(x) => handleSubmit()}>
                    <Save /> Lưu
                  </Button>
                </Modal.Footer>
              </>
            )}
          </WorkflowCategoryEditor>
        </Modal>

        {/* Delete modal */}
        <Modal
          show={isDeleteModalOpen}
          onHide={() => this.toggleDeleteWorkflowCategoryModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Xóa loại quy trìnhề:{" "}
              {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Bạn có chắc chắn muốn xóa loại quy trình này không?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => this.toggleDeleteWorkflowCategoryModal()}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button
              variant="danger"
              onClick={(x) => this.deleteWorkflowCategory()}
            >
              <Trash /> Xóa
            </Button>
          </Modal.Footer>
        </Modal>

        <Paginator
          ref={(x) => (this.paginator = x)}
          totalResults={items.length}
          limitPerPage={limitPerPage}
          currentPage={currentPageNum}
          onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
        />
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  WorkflowCategoryPage,
  (state) => state.workflowCategory, // Selects which state properties are merged into the component's props.
  WorkflowCategoryStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
