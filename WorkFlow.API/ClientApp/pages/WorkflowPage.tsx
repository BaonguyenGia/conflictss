﻿import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { IWorkflowModel } from "@Models/IWorkflowModel";
import * as WorkflowStore from "@Store/workflowStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import { LinkContainer } from "react-router-bootstrap";
import {
  Modal,
  Button,
  Container,
  Row,
  Card,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import {
  XCircle,
  Trash,
  X,
  ArrowClockwise,
  ThreeDots,
  Plus,
} from "react-bootstrap-icons";

type Props = typeof WorkflowStore.actionCreators &
  WorkflowStore.IWorkflowStoreState &
  RouteComponentProps<{}>;

interface IState {
  searchTerm: string;
  currentPageNum: number;
  limitPerPage: number;
  isDeleteModalOpen: boolean;
  modelForEdit?: IWorkflowModel;
}

class WorkflowPage extends React.Component<Props, IState> {
  private paginator: Paginator;

  private debouncedSearch: (term: string) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchTerm: "",
      currentPageNum: 1,
      limitPerPage: 14,
      modelForEdit: null,
      isDeleteModalOpen: false,
    };

    // "AwesomeDebouncePromise" makes a delay between
    // the end of input term and search request.
    this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
      props.search(term);
    }, 500);
    this.props.search();
    this.props.searchWorkflowCategory();
  }

  private refresh = () => {
    this.setState({ currentPageNum: 1 });
    wait(async () => {
      await this.props.search(this.state.searchTerm);
    }, "WorkflowCategoryPageTask");
  };

  //get Category Name by Id
  private renderWorkflowCategory = (id: number | null) => {
    const { workflowCategories } = this.props;
    var categoryTitle = "";
    if (id) {
      var findIndex = workflowCategories.findIndex((x) => x.id === id);
      if (findIndex >= 0) {
        categoryTitle = workflowCategories[findIndex].title;
      }
    }
    return categoryTitle;
  };

  //get status by identifed number
  private getStatus(id) {
    let result;
    id == 1
      ? (result = "Active")
      : id == 2
      ? (result = "Deactive")
      : id == 3
      ? (result = "Draft")
      : (result = "");
    return result;
  }

  private toggleModel = (modelForEdit?: IWorkflowModel) => {
    this.setState((prev) => ({
      modelForEdit,
    }));
  };

  private toggleDeleteWorkflowModal = () => {
    const { modelForEdit } = this.state;
    if (modelForEdit == null) {
      return;
    }
    this.setState((prev) => ({
      modelForEdit: prev.isDeleteModalOpen === true ? null : prev.modelForEdit,
      isDeleteModalOpen: !prev.isDeleteModalOpen,
    }));
  };

  private deleteWorkflow = async () => {
    const { remove } = this.props;
    const { modelForEdit } = this.state;
    await remove(modelForEdit.id);
    this.toggleDeleteWorkflowModal();
  };

  private renderRows = (data: IWorkflowModel[]) => {
    const { currentPageNum, limitPerPage, modelForEdit } = this.state;

    return paginate(data, currentPageNum, limitPerPage).map((item) => (
      <tr
        key={item.id}
        className={`subrow
                ${
                  modelForEdit && item.id === modelForEdit.id
                    ? " tr-selected"
                    : ""
                }`}
        onClick={() => this.toggleModel(item)}
      >
        <td>
          <a className="subtitle" href="javascript:void(0)">
            {item.title}
          </a>
        </td>
        <td>{item.code}</td>
        <td>{this.renderWorkflowCategory(item.categoryId)}</td>
        <td>{item.startDate}</td>
        <td>{item.endDate}</td>
        <td>{item.version}</td>
        <td>{item.createPermission}</td>
        <td>{this.getStatus(item.status)}</td>
      </tr>
    ));
  };

  private onClearSearchInput = () => {
    var val = "";
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    var val = e.currentTarget.value;
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  render() {
    const { items, isFetching } = this.props;
    const {
      modelForEdit,
      isDeleteModalOpen,
      limitPerPage,
      currentPageNum,
      searchTerm,
    } = this.state;

    return (
      <Container>
        <Helmet>
          <title>Chức vụ - All Items</title>
        </Helmet>

        <Card body className="mt-4 mb-4">
          <Row>
            <div className="col-8 col-sm-9 col-md-9 col-lg-8">
              <InputGroup className="mb-3">
                <FormControl
                  placeholder={"Tìm kiếm theo tiêu đề..."}
                  defaultValue={""}
                  value={searchTerm}
                  onChange={this.onChangeSearchInput}
                />
                {searchTerm.trim().length > 0 && (
                  <Button variant="secondary" onClick={this.onClearSearchInput}>
                    <X />
                  </Button>
                )}
              </InputGroup>
            </div>
            <div className="col-4 col-sm-3 col-md-3 col-lg-4">
              <Button
                className="button-reload"
                disabled={isFetching}
                onClick={(x) => this.refresh()}
              >
                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
              </Button>
            </div>
          </Row>
        </Card>

        <div className="option-panel">
          <LinkContainer exact to={"/workflows/newform"}>
            <div className="add">
              <Plus size={30} color="green" /> Tạo mới
            </div>
          </LinkContainer>

          <div onClick={() => this.toggleDeleteWorkflowModal()}>
            <Trash size={20} color="red" /> Xóa
          </div>
        </div>

        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Mã</th>
              <th>Category</th>
              <th>Ngày bắt đầu</th>
              <th>Ngày kết thúc</th>
              <th>Phiên Bản Quy Trình</th>
              <th>Quyền tạo</th>
              <th>Tình trạng</th>
            </tr>
          </thead>
          <tbody>{this.renderRows(items)}</tbody>
        </table>

        {/* Delete modal */}
        <Modal
          show={isDeleteModalOpen}
          onHide={() => this.toggleDeleteWorkflowModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Xóa quy trình: {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Bạn có chắc chắn muốn xóa quy trình này không?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => this.toggleDeleteWorkflowModal()}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button variant="danger" onClick={(x) => this.deleteWorkflow()}>
              <Trash /> Xóa
            </Button>
          </Modal.Footer>
        </Modal>

        <Paginator
          ref={(x) => (this.paginator = x)}
          totalResults={items.length}
          limitPerPage={limitPerPage}
          currentPage={currentPageNum}
          onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
        />
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  WorkflowPage,
  (state) => state.workflow, // Selects which state properties are merged into the component's props.
  WorkflowStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
