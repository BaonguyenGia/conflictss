﻿import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";
import { ISupportContactModel } from "@Models/ISupportContactModel";
import * as supportContactStore from "@Store/supportContactStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { formatTimeInEntity } from "@Services/FormatDateTimeService";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import SupportContactEditor from "@Components/supportContact/SupportContactEditor";
import {
  Modal,
  Button,
  Container,
  Row,
  Card,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import Result from "@Core/Result";
import {
  Save,
  XCircle,
  Trash,
  PlusSquare,
  X,
  ArrowClockwise,
  ThreeDots,
  Plus,
} from "react-bootstrap-icons";

type Props = typeof supportContactStore.actionCreators &
  supportContactStore.ISupportContactStoreState &
  RouteComponentProps<{}>;

interface IState {
  searchTerm: string;
  currentPageNum: number;
  limitPerPage: number;
  isAddModalOpen: boolean;
  isUpdateModalOpen: boolean;
  isDeleteModalOpen: boolean;
  modelForEdit?: ISupportContactModel;
}

class SupportContactPage extends React.Component<Props, IState> {
  private paginator: Paginator;

  private debouncedSearch: (term: string) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchTerm: "",
      currentPageNum: 1,
      limitPerPage: 14,
      modelForEdit: null,
      isAddModalOpen: false,
      isDeleteModalOpen: false,
      isUpdateModalOpen: false,
    };

    this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
      props.search(term);
    }, 500);

    this.props.search();
    this.props.searchSubData();
  }

  private refresh = () => {
    this.setState({ currentPageNum: 1 });
    wait(async () => {
      await this.props.search();
    }, "WorkflowCategoryPageTask");
  };
  private toggleModelForEdit = (modelForEdit?: ISupportContactModel) => {
    this.setState((prev) => ({
      modelForEdit,
    }));
  };

  private toggleUpdateSupportContactModal = (
    modelForEdit?: ISupportContactModel
  ) => {
    this.setState((prev) => ({
      modelForEdit,
      isUpdateModalOpen: !prev.isUpdateModalOpen,
    }));
  };

  private toggleAddSupportContactModal = () => {
    this.setState((prev) => ({
      isAddModalOpen: !prev.isAddModalOpen,
    }));
  };

  private toggleDeleteSupportContactModal = () => {
    const { modelForEdit } = this.state;
    if (modelForEdit == null) {
      return;
    }
    this.setState((prev) => ({
      modelForEdit: prev.isDeleteModalOpen === true ? null : prev.modelForEdit,
      isDeleteModalOpen: !prev.isDeleteModalOpen,
    }));
  };

  //get Name by Id
  private renderPersonName = (id: number | null) => {
    const { personalProfileOptions } = this.props;
    var categoryTitle = "";
    if (id) {
      var findIndex = personalProfileOptions.findIndex((x) => x.id === id);
      if (findIndex >= 0) {
        categoryTitle = personalProfileOptions[findIndex].fullName;
      }
    }
    return categoryTitle;
  };

  private addSupportContact = async (data: ISupportContactModel) => {
    const { add } = this.props;
    var result = (await add(data)) as any as Result<ISupportContactModel>;

    if (!result.hasErrors) {
      this.paginator.setLastPage();
      this.toggleAddSupportContactModal();
    }
  };

  private updateSupportContact = async (data: ISupportContactModel) => {
    const { update } = this.props;
    var result = await getPromiseFromActionCreator(update(data));
    if (!result.hasErrors) {
      this.toggleUpdateSupportContactModal();
    }
  };

  private deleteSupportContact = async () => {
    const { remove } = this.props;
    const { modelForEdit } = this.state;
    await remove(modelForEdit.id);
    this.toggleDeleteSupportContactModal();
  };

  private renderRows = (data: ISupportContactModel[]) => {
    const { currentPageNum, limitPerPage, modelForEdit } = this.state;

    return paginate(data, currentPageNum, limitPerPage).map((item) => (
      <tr
        key={item.id}
        className={`subrow
          ${modelForEdit && item.id === modelForEdit.id ? " tr-selected" : ""}`}
        onClick={() => this.toggleModelForEdit(item)}
      >
        <td>
          <a
            className="subtitle"
            href="javascript:void(0)"
            onClick={(x) => this.toggleUpdateSupportContactModal(item)}
          >
            {item.title}
          </a>
        </td>
        <td>{item.phoneNumber}</td>
        <td>{this.renderPersonName(item.personalProfileId)}</td>
        <td>
          {item.lastModified
            ? formatTimeInEntity(item.lastModified)
            : formatTimeInEntity(item.createdTime)}
        </td>
      </tr>
    ));
  };

  private onClearSearchInput = () => {
    var val = "";
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    var val = e.currentTarget.value;
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  render() {
    const { items, isFetching, personalProfileOptions } = this.props;
    const {
      isAddModalOpen,
      isUpdateModalOpen,
      modelForEdit,
      isDeleteModalOpen,
      limitPerPage,
      currentPageNum,
      searchTerm,
    } = this.state;

    return (
      <Container>
        <Helmet>
          <title>Hỗ trợ - All Items</title>
        </Helmet>

        <Card body className="mt-4 mb-4">
          <Row>
            <div className="col-8 col-sm-9 col-md-9 col-lg-8">
              <InputGroup className="mb-3">
                <FormControl
                  placeholder={"Tìm kiếm theo tiêu đề..."}
                  defaultValue={""}
                  value={searchTerm}
                  onChange={this.onChangeSearchInput}
                />
                {searchTerm.trim().length > 0 && (
                  <Button variant="secondary" onClick={this.onClearSearchInput}>
                    <X />
                  </Button>
                )}
              </InputGroup>
            </div>
            <div className="col-4 col-sm-3 col-md-3 col-lg-4">
              <Button
                className="button-reload"
                disabled={isFetching}
                onClick={(x) => this.refresh()}
              >
                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
              </Button>
            </div>
          </Row>
        </Card>
        <div className="option-panel">
          <div
            className="add"
            onClick={() => this.toggleAddSupportContactModal()}
          >
            <Plus size={30} color="green" /> Tạo mới
          </div>
          <div onClick={() => this.toggleDeleteSupportContactModal()}>
            <Trash size={20} color="red" /> Xóa
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Số điện thoại</th>
              <th>Tài khoản</th>
              <th>Đã sửa đổi</th>
            </tr>
          </thead>
          <tbody>{this.renderRows(items)}</tbody>
        </table>
        {/* Add modal */}
        <Modal
          show={isAddModalOpen}
          onHide={() => this.toggleAddSupportContactModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>Thêm mới</Modal.Title>
          </Modal.Header>

          <SupportContactEditor
            data={{} as ISupportContactModel}
            onSubmit={this.addSupportContact}
            isEditted={false}
            personalProfileOptions={personalProfileOptions}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleAddSupportContactModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="success" onClick={(x) => handleSubmit()}>
                    <PlusSquare /> Thêm
                  </Button>
                </Modal.Footer>
              </>
            )}
          </SupportContactEditor>
        </Modal>
        {/* Update modal */}
        <Modal
          show={isUpdateModalOpen}
          onHide={() => this.toggleUpdateSupportContactModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Chỉnh sửa liên hệ: {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>

          <SupportContactEditor
            data={modelForEdit}
            onSubmit={this.updateSupportContact}
            isEditted={true}
            personalProfileOptions={personalProfileOptions}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>

                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleUpdateSupportContactModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="primary" onClick={(x) => handleSubmit()}>
                    <Save /> Lưu
                  </Button>
                </Modal.Footer>
              </>
            )}
          </SupportContactEditor>
        </Modal>
        {/* Delete modal */}
        <Modal
          show={isDeleteModalOpen}
          onHide={() => this.toggleDeleteSupportContactModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Xóa trợ giúp: {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Bạn có chắc chắn muốn xóa trợ giúp này không?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => this.toggleDeleteSupportContactModal()}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button
              variant="danger"
              onClick={(x) => this.deleteSupportContact()}
            >
              <Trash /> Xóa
            </Button>
          </Modal.Footer>
        </Modal>

        <Paginator
          ref={(x) => (this.paginator = x)}
          totalResults={items.length}
          limitPerPage={limitPerPage}
          currentPage={currentPageNum}
          onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
        />
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  SupportContactPage,
  (state) => state.supportContact, // Selects which state properties are merged into the component's props.
  supportContactStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
