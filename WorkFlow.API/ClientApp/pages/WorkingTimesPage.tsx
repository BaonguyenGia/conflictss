import * as React from "react";
import { Helmet } from "react-helmet";
import { RouteComponentProps, withRouter } from "react-router";

import { IWorkingTimesModel } from "@Models/IWorkingTimesModel";
import * as workingTimesStore from "@Store/workingTimesStore";
import { withStore } from "@Store/index";
import Paginator from "@Components/shared/Paginator";
import WorkingTimesEditor from "@Components/workingTimes/workingTimesEditor";
import AwesomeDebouncePromise from "awesome-debounce-promise";
import { paginate, getPromiseFromActionCreator } from "@Utils";
import {
  Modal,
  Button,
  Container,
  Row,
  Card,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { wait } from "domain-wait";
import Result from "@Core/Result";
import {
  Pencil,
  Trash,
  Save,
  XCircle,
  Trash2,
  PlusSquare,
  X,
  ArrowClockwise,
  ThreeDots,
  Plus,
} from "react-bootstrap-icons";

type Props = typeof workingTimesStore.actionCreators &
  workingTimesStore.IWorkingTimesStoreState &
  RouteComponentProps<{}>;

interface IState {
  searchTerm: string;
  currentPageNum: number;
  limitPerPage: number;
  isAddModalOpen: boolean;
  isUpdateModalOpen: boolean;
  isDeleteModalOpen: boolean;
  modelForEdit?: IWorkingTimesModel;
}

class WorkingTimesPage extends React.Component<Props, IState> {
  private paginator: Paginator;

  private debouncedSearch: (term: string) => void;

  constructor(props: Props) {
    super(props);

    this.state = {
      searchTerm: "",
      currentPageNum: 1,
      limitPerPage: 8,
      modelForEdit: null,
      isAddModalOpen: false,
      isDeleteModalOpen: false,
      isUpdateModalOpen: false,
    };

    // "AwesomeDebouncePromise" makes a delay between
    // the end of input term and search request.
    this.debouncedSearch = AwesomeDebouncePromise((term: string) => {
      props.search(term);
    }, 500);

    wait(async () => {
      // Lets tell Node.js to wait for the request completion.
      // It's necessary when you want to see the fethched data
      // in your prerendered HTML code (by SSR).
      await this.props.search();
    }, "positionPageTask");
  }

  private refresh = () => {
    this.setState({ currentPageNum: 1 });
    wait(async () => {
      await this.props.search(this.state.searchTerm);
    }, "WorkflowCategoryPageTask");
  };

  private toggleWorkingTimesModal = () => {
    this.setState((prev) => ({
      isAddModalOpen: !prev.isAddModalOpen,
    }));
  };

  private toggleUpdateWorkingTimesModal = (
    modelForEdit?: IWorkingTimesModel
  ) => {
    this.setState((prev) => ({
      modelForEdit,
      isUpdateModalOpen: !prev.isUpdateModalOpen,
    }));
  };

  private toggleDeleteWorkingTimesModal = () => {
    const { modelForEdit } = this.state;
    if (modelForEdit == null) {
      return;
    }
    this.setState((prev) => ({
      modelForEdit,
      isDeleteModalOpen: !prev.isDeleteModalOpen,
    }));
  };

  private addWorkingTimes = async (data: IWorkingTimesModel) => {
    const { add } = this.props;
    var result = (await add(data)) as any as Result<IWorkingTimesModel>;

    if (!result.hasErrors) {
      this.paginator.setLastPage();
      this.toggleWorkingTimesModal();
    }
  };

  private updateWorkingTimes = async (data: IWorkingTimesModel) => {
    const { update } = this.props;
    var result = await getPromiseFromActionCreator(update(data));

    if (!result.hasErrors) {
      this.toggleUpdateWorkingTimesModal();
    }
  };

  private deleteWorkingTimes = async () => {
    const { remove } = this.props;
    const { modelForEdit } = this.state;

    await remove(modelForEdit.id);
    this.toggleDeleteWorkingTimesModal();
  };
  private toggleModelForEdit = (modelForEdit?: IWorkingTimesModel) => {
    this.setState((prev) => ({
      modelForEdit,
    }));
  };

  private renderRows = (data: IWorkingTimesModel[]) => {
    const { currentPageNum, limitPerPage, modelForEdit } = this.state;

    return paginate(data, currentPageNum, limitPerPage).map((item) => (
      <tr
        id="workingTimes-table"
        key={item.id}
        className={`subrow
          ${modelForEdit && item.id === modelForEdit.id ? " tr-selected" : ""}`}
        onClick={() => this.toggleModelForEdit(item)}
      >
        <td>
          <a
            className="subtitle"
            href="javascript:void(0)"
            onClick={(x) => this.toggleUpdateWorkingTimesModal(item)}
          >
            {item.title}
          </a>
        </td>
        <td id="td-child">{item.morningStart}</td>
        <td id="td-child">{item.morningEnd}</td>
        <td id="td-child">{item.afternoonStart}</td>
        <td id="td-child">{item.afternoonEnd}</td>
      </tr>
    ));
  };

  private onClearSearchInput = () => {
    var val = "";
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  private onChangeSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    var val = e.currentTarget.value;
    this.debouncedSearch(val);
    this.paginator.setFirstPage();
    this.setState({ searchTerm: val });
  };

  render() {
    const { items, isFetching } = this.props;
    const {
      isAddModalOpen,
      isUpdateModalOpen,
      modelForEdit,
      isDeleteModalOpen,
      limitPerPage,
      currentPageNum,
      searchTerm,
    } = this.state;

    return (
      <Container>
        <Helmet>
          <title>Cấu hình thời gian làm việc</title>
        </Helmet>

        <Card body className="mt-4 mb-4">
          <Row>
            <div className="col-8 col-sm-9 col-md-9 col-lg-8">
              <InputGroup className="mb-3">
                <FormControl
                  placeholder={"Tìm kiếm theo tiêu đề..."}
                  defaultValue={""}
                  value={searchTerm}
                  onChange={this.onChangeSearchInput}
                />
                {searchTerm.trim().length > 0 && (
                  <Button variant="secondary" onClick={this.onClearSearchInput}>
                    <X />
                  </Button>
                )}
              </InputGroup>
            </div>
            <div className="col-4 col-sm-3 col-md-3 col-lg-4">
              <Button
                className="button-reload"
                disabled={isFetching}
                onClick={(x) => this.refresh()}
              >
                {isFetching ? <ThreeDots /> : <ArrowClockwise />}
              </Button>
            </div>
          </Row>
        </Card>
        <div className="option-panel">
          <div onClick={() => this.toggleWorkingTimesModal()} className="add">
            <Plus size={30} color="green" /> Tạo mới
          </div>
          <div onClick={() => this.toggleDeleteWorkingTimesModal()}>
            <Trash size={20} color="red" /> Xóa
          </div>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th>Tiêu đề</th>
              <th>Thời gian bắt đầu buổi sáng</th>
              <th>Thời gian kết thúc buổi sáng</th>
              <th>Thời gian bắt đầu buổi chiều</th>
              <th>Thời gian kết thúc buổi chiều</th>
            </tr>
          </thead>
          <tbody>{this.renderRows(items)}</tbody>
        </table>

        {/* Add modal */}
        <Modal
          show={isAddModalOpen}
          onHide={() => this.toggleWorkingTimesModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>Thêm mới</Modal.Title>
          </Modal.Header>

          <WorkingTimesEditor
            data={{} as IWorkingTimesModel}
            onSubmit={this.addWorkingTimes}
            isEditted={false}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleWorkingTimesModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="success" onClick={(x) => handleSubmit()}>
                    <PlusSquare /> Thêm
                  </Button>
                </Modal.Footer>
              </>
            )}
          </WorkingTimesEditor>
        </Modal>

        {/* Update modal */}
        <Modal
          show={isUpdateModalOpen}
          onHide={() => this.toggleUpdateWorkingTimesModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Chỉnh sửa thời gian làm việc:{" "}
              {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>

          <WorkingTimesEditor
            data={modelForEdit}
            onSubmit={this.updateWorkingTimes}
            isEditted={true}
          >
            {(renderEditor, handleSubmit) => (
              <>
                <Modal.Body>{renderEditor()}</Modal.Body>
                <Modal.Footer>
                  <Button
                    variant="secondary"
                    onClick={(x) => this.toggleUpdateWorkingTimesModal()}
                  >
                    <XCircle /> Hủy bỏ
                  </Button>
                  <Button variant="primary" onClick={(x) => handleSubmit()}>
                    <Save /> Lưu
                  </Button>
                </Modal.Footer>
              </>
            )}
          </WorkingTimesEditor>
        </Modal>

        {/* Delete modal */}
        <Modal
          show={isDeleteModalOpen}
          onHide={() => this.toggleDeleteWorkingTimesModal()}
        >
          <Modal.Header closeButton>
            <Modal.Title>
              Xóa thời gian làm việc:{" "}
              {modelForEdit ? `${modelForEdit.title}` : null}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Bạn có chắc chắn muốn xóa?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={(x) => this.toggleDeleteWorkingTimesModal()}
            >
              <XCircle /> Hủy bỏ
            </Button>
            <Button variant="danger" onClick={(x) => this.deleteWorkingTimes()}>
              <Trash2 /> Xóa
            </Button>
          </Modal.Footer>
        </Modal>

        <Paginator
          ref={(x) => (this.paginator = x)}
          totalResults={items.length}
          limitPerPage={limitPerPage}
          currentPage={currentPageNum}
          onChangePage={(pageNum) => this.setState({ currentPageNum: pageNum })}
        />
      </Container>
    );
  }
}

// Connect component with Redux store.
var connectedComponent = withStore(
  WorkingTimesPage,
  (state) => state.workingTimes, // Selects which state properties are merged into the component's props.
  workingTimesStore.actionCreators // Selects which action creators are merged into the component's props.
);

// Attach the React Router to the component to have an opportunity
// to interract with it: use some navigation components,
// have an access to React Router fields in the component's props, etc.
export default withRouter(connectedComponent);
