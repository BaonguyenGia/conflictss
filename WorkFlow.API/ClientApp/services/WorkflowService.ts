﻿import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IWorkflowModel } from "@Models/IWorkflowModel";
import { IQueryResult } from "@Models/IQueryResult";

export default class WorkflowService extends ServiceBase {
    public async search(
        title: string = null
    ): Promise<Result<IQueryResult<IWorkflowModel>>> {
        if (title == null) {
            title = "";
        }
        var result = await this.requestJson<IQueryResult<IWorkflowModel>>({
            url: `/api/workflows`,
            method: "GET",
            data: { title: title },
        });
        return result;
    }

    public async update(model: IWorkflowModel): Promise<Result<IWorkflowModel>> {
        var result = await this.requestJson({
            url: `/api/workflows/${model.id}`,
            method: "PUT",
            data: model,
        });
        return result;
    }

    public async delete(id: number): Promise<Result<{}>> {
        var result = await this.requestJson({
            url: `/api/workflows/${id}`,
            method: "DELETE",
        });
        return result;
    }

    public async add(model: IWorkflowModel): Promise<Result<IWorkflowModel>> {
        var result = await this.requestJson<number>({
            url: "/api/workflows",
            method: "POST",
            data: model,
        });
        return result;
    }
}