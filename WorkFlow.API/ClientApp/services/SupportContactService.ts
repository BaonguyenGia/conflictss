﻿import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { ISupportContactModel } from "@Models/ISupportContactModel";
import { IQueryResult } from "@Models/IQueryResult";

export default class SupportContactService extends ServiceBase {
    public async search(
        title: string = null
    ): Promise<Result<IQueryResult<ISupportContactModel>>> {
        if (title == null) {
            title = "";
        }
        var result = await this.requestJson<IQueryResult<ISupportContactModel>>({
            url: `/api/supportcontacts`,
            method: "GET",
            data: { title: title },
        });
        return result;
    }

    public async update(model: ISupportContactModel): Promise<Result<ISupportContactModel>> {
        var result = await this.requestJson({
            url: `/api/supportcontacts/${model.id}`,
            method: "PUT",
            data: model,
        });
        return result;
    }

    public async delete(id: number): Promise<Result<{}>> {
        var result = await this.requestJson({
            url: `/api/supportcontacts/${id}`,
            method: "DELETE",
        });
        return result;
    }

    public async add(model: ISupportContactModel): Promise<Result<ISupportContactModel>> {
        var result = await this.requestJson<number>({
            url: "/api/supportcontacts",
            method: "POST",
            data: model,
        });
        return result;
    }
}
