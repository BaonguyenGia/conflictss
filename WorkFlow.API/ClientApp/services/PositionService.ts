import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IPositionModel } from "@Models/IPositionModel";
import { IQueryResult } from "@Models/IQueryResult";

export default class PositionService extends ServiceBase {
  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IPositionModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IPositionModel>>({
      url: `/api/positions`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }

  public async update(model: IPositionModel): Promise<Result<IPositionModel>> {
    var result = await this.requestJson({
      url: `/api/positions/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/positions/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IPositionModel): Promise<Result<IPositionModel>> {
    var result = await this.requestJson<number>({
      url: "/api/positions",
      method: "POST",
      data: model,
    });
    return result;
  }
}
