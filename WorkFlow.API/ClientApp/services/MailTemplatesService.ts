import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IMailTemplatesModel } from "@Models/IMailTemplatesModel";

import { IQueryResult } from "@Models/IQueryResult";

export default class MailTemplatesService extends ServiceBase {
  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IMailTemplatesModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IMailTemplatesModel>>({
      url: `/api/mailTemplates`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }

  public async update(model: IMailTemplatesModel): Promise<Result<IMailTemplatesModel>> {
    var result = await this.requestJson({
      url: `/api/mailTemplates/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/mailTemplates/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IMailTemplatesModel): Promise<Result<IMailTemplatesModel>> {
    var result = await this.requestJson<number>({
      url: "/api/mailTemplates",
      method: "POST",
      data: model,
    });
    return result;
  }
}
