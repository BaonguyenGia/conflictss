import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IDepartmentModel } from "@Models/IDepartmentModel";
import { IQueryResult } from "@Models/IQueryResult";
import { IDepartmentTreeViewModel } from "@Models/IDepartmentTreeViewModel";

export default class DepartmentService extends ServiceBase {
  public async searchDepartmentTreeView(): Promise<
    Result<IDepartmentTreeViewModel>
  > {
    var result = await this.requestJson<IQueryResult<IDepartmentModel>>({
      url: `/api/departments/treeview`,
      method: "GET",
      data: {},
    });
    return result;
  }

  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IDepartmentModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IDepartmentModel>>({
      url: `/api/departments`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }

  public async update(model: IDepartmentModel): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/departments/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/departments/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IDepartmentModel): Promise<Result<number>> {
    var result = await this.requestJson<number>({
      url: "/api/departments",
      method: "POST",
      data: model,
    });
    return result;
  }
}
