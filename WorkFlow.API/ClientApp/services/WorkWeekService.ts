import Result from "@Core/Result";
import { ServiceBase } from "@Core/ServiceBase";
import { IWorkWeekModel } from "@Models/IWorkWeekModel";

import { IQueryResult } from "@Models/IQueryResult";

export default class WorkWeekService extends ServiceBase {
  public async search(
    title: string = null
  ): Promise<Result<IQueryResult<IWorkWeekModel>>> {
    if (title == null) {
      title = "";
    }
    var result = await this.requestJson<IQueryResult<IWorkWeekModel>>({
      url: `/api/workweeks`,
      method: "GET",
      data: { title: title },
    });
    return result;
  }

  public async update(model: IWorkWeekModel): Promise<Result<IWorkWeekModel>> {
    var result = await this.requestJson({
      url: `/api/workweeks/${model.id}`,
      method: "PUT",
      data: model,
    });
    return result;
  }

  public async delete(id: number): Promise<Result<{}>> {
    var result = await this.requestJson({
      url: `/api/workweeks/${id}`,
      method: "DELETE",
    });
    return result;
  }

  public async add(model: IWorkWeekModel): Promise<Result<IWorkWeekModel>> {
    var result = await this.requestJson<number>({
      url: "/api/workweeks",
      method: "POST",
      data: model,
    });
    return result;
  }
}
