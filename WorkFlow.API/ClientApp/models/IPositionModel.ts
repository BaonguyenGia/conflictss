import { IEngineEntity } from "@Models/IEngineEntity";

export interface IPositionModel extends IEngineEntity {
    title: string;
    code: string;
    description: string;
}