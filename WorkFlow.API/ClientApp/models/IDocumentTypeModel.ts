﻿import { IEngineEntity } from "@Models/IEngineEntity";

export interface IDocumentTypeModel extends IEngineEntity {
    title: string;
    code: string;
    description: string;
}