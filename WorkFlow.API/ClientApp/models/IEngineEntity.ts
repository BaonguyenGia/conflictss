export interface IEngineEntity {
    id: number;
    createdBy: string;
    createdTime: string | null;
    lastModified: string | null;
    modifiedBy: string;
    isDeleted: boolean;
}