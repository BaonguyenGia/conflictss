import { IEngineEntity } from "@Models/IEngineEntity";

export interface IDepartmentModel extends IEngineEntity {
  title: string;
  label: string;
  code: string;
  description: string;
  parentId: number | null;
  chartCode: string;
  managerId: number | null;
  deptLevel: number;
  email: string;
  telephone: string;
  fax: string;
  address: string;
  siteName: string;
  order: number | null;
}
