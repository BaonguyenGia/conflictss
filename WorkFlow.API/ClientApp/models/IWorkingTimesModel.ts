import { IEngineEntity } from "@Models/IEngineEntity";


export interface IWorkingTimesModel extends IEngineEntity {
    title: string;
    workweekId: number;
    morningStart: number;
    morningEnd: number;
    afternoonStart: number;
    afternoonEnd: number;
}