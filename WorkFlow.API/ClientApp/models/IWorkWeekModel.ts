import { IEngineEntity } from "@Models/IEngineEntity";


export interface IWorkWeekModel extends IEngineEntity {
    title: string;
    isFullTime: boolean; 
}