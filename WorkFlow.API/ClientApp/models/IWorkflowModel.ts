﻿import { IEngineEntity } from "@Models/IEngineEntity";

export interface IWorkflowModel extends IEngineEntity {
  title: string;
  code: string;
  status: IWorkflowStatus;
  version: number;
  createPermission: string;
  seenPermission: string;
  imageURL: string | null;
  isAttachmentActive: boolean;
  allowAttachmentExtensions: string;
  maximumSizeSingle: number;
  maximumSizeTotal: number;
  categoryId: number;
  startDate: Date | null;
  endDate: Date | null;
  order: number;
}
