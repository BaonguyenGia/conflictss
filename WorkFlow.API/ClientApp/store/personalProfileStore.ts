import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import PersonalProfileService from "@Services/PersonalProfileService";
import PositionService from "@Services/PositionService";
import { IPositionModel } from "@Models/IPositionModel";
import DepartmentService from "@Services/DepartmentService";
import { IDepartmentModel } from "@Models/IDepartmentModel";
import { IDepartmentTreeViewModel } from "@Models/IDepartmentTreeViewModel";

// Declare an interface of the store's state.
export interface IPersonalProfileStoreState {
  isFetching: boolean;
  items: IPersonalProfileModel[];
  positionOptions: IPositionModel[];
  departmentOptions: IDepartmentModel[];
  departmentTreeViews: IDepartmentTreeViewModel[];
  personalProfileOptions: IPersonalProfileModel[];
}

export interface IPersonalProfileSubData {
  positionOptions: IPositionModel[];
  departmentOptions: IDepartmentModel[];
  departmentTreeViews: IDepartmentTreeViewModel[];
}

// Create the slice.
const slice = createSlice({
  name: "personalProfile",
  initialState: {
    isFetching: false,
    items: [],
    positionOptions: [],
  } as IPersonalProfileStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IPersonalProfileModel[]>) => {
      state.items = action.payload;
      state.personalProfileOptions = action.payload;
    },
    setPositions: (state, action: PayloadAction<IPositionModel[]>) => {
      state.positionOptions = action.payload;
    },
    setDepartment: (state, action: PayloadAction<IDepartmentModel[]>) => {
      state.departmentOptions = action.payload;
    },
    setSubData: (state, action: PayloadAction<IPersonalProfileSubData>) => {
      state.departmentOptions = action.payload.departmentOptions;
      state.positionOptions = action.payload.positionOptions;
      state.departmentTreeViews = action.payload.departmentTreeViews;
    },
    addData: (state, action: PayloadAction<IPersonalProfileModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IPersonalProfileModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (fullnameoremail?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.search(fullnameoremail);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  searchSubData: () => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const positionService = new PositionService();
    const departmentnService = new DepartmentService();

    const positionResult = await positionService.search();
    const departmentResult = await departmentnService.search();
    const departmentTreeViewResult =
      await departmentnService.searchDepartmentTreeView();

    if (
      !positionResult.hasErrors &&
      !departmentResult.hasErrors &&
      !departmentTreeViewResult.hasErrors
    ) {
      const subData: IPersonalProfileSubData = {
        positionOptions: positionResult.value.items,
        departmentOptions: departmentResult.value.items,
        departmentTreeViews: departmentTreeViewResult.value,
      };

      dispatch(slice.actions.setSubData(subData));
    }

    dispatch(slice.actions.setFetching(false));
  },
  add: (model: IPersonalProfileModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.add(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.addData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  update:
    (model: IPersonalProfileModel, avatar: File | null) =>
    async (dispatch: Dispatch) => {
      dispatch(slice.actions.setFetching(true));

      const service = new PersonalProfileService();

      const result = await service.update(model);

      if (!result.hasErrors) {
        if (avatar) {
          await service.uploadAvatar(result.value.id, avatar);
        }
        dispatch(slice.actions.updateData(result.value));
      }

      dispatch(slice.actions.setFetching(false));

      return result;
    },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
