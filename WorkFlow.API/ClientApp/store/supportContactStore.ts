﻿import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { ISupportContactModel } from "@Models/ISupportContactModel";
import SupportContactService from "@Services/SupportContactService";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";
import PersonalProfileService from "@Services/PersonalProfileService";

// Declare an interface of the store's state.
export interface ISupportContactStoreState {
  isFetching: boolean;
  items: ISupportContactModel[];
  personalProfileOptions: IPersonalProfileModel[];
}
export interface ISupportContactSubData {
  personalProfileOptions: IPersonalProfileModel[];
}

// Create the slice.
const slice = createSlice({
  name: "supportContact",
  initialState: {
    isFetching: false,
    items: [],
    personalProfileOptions: [],
  } as ISupportContactStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<ISupportContactModel[]>) => {
      state.items = action.payload;
    },
    addData: (state, action: PayloadAction<ISupportContactModel>) => {
      state.items = [...state.items, action.payload];
    },
    setPersonalProfile: (
      state,
      action: PayloadAction<IPersonalProfileModel[]>
    ) => {
      state.personalProfileOptions = action.payload;
    },
    setSubData: (state, action: PayloadAction<ISupportContactSubData>) => {
      state.personalProfileOptions = action.payload.personalProfileOptions;
    },
    updateData: (state, action: PayloadAction<ISupportContactModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));
    const service = new SupportContactService();
    const result = await service.search(title);
    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }
    dispatch(slice.actions.setFetching(false));
    return result;
  },

  searchSubData: () => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const personalProfileService = new PersonalProfileService();
    const personalProfileResult = await personalProfileService.search();

    if (!personalProfileResult.hasErrors) {
      const subData: ISupportContactSubData = {
        personalProfileOptions: personalProfileResult.value.items,
      };
      dispatch(slice.actions.setSubData(subData));
    }
    dispatch(slice.actions.setFetching(false));
  },
  add: (model: ISupportContactModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new SupportContactService();
    const result = await service.add(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.addData(result.value));
    }
    dispatch(slice.actions.setFetching(false));
    return result;
  },
  update: (model: ISupportContactModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new SupportContactService();
    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }
    dispatch(slice.actions.setFetching(false));
    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new SupportContactService();
    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }
    dispatch(slice.actions.setFetching(false));
    return result;
  },
};
