﻿import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IDocumentTypeModel } from "@Models/IDocumentTypeModel";
import DocumentTypeService from "@Services/DocumentTypeService";

// Declare an interface of the store's state.
export interface IDocumentTypeStoreState {
  isFetching: boolean;
  items: IDocumentTypeModel[];
}

// Create the slice.
const slice = createSlice({
  name: "documentType",
  initialState: {
    isFetching: false,
    items: [],
  } as IDocumentTypeStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IDocumentTypeModel[]>) => {
      state.items = action.payload;
    },
    addData: (state, action: PayloadAction<IDocumentTypeModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IDocumentTypeModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DocumentTypeService();

    const result = await service.search(title);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  add: (model: IDocumentTypeModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DocumentTypeService();

    const result = await service.add(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.addData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  update: (model: IDocumentTypeModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DocumentTypeService();

    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new DocumentTypeService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
