﻿import { createSlice, PayloadAction, Dispatch } from "@reduxjs/toolkit";
import { IWorkflowModel } from "@Models/IWorkflowModel";
import WorkflowService from "@Services/WorkflowService";
import { IWorkflowCategoryModel } from "@Models/IWorkflowCategoryModel";
import WorkflowCategoryService from "../services/WorkflowCategoryService";
import PersonalProfileService from "@Services/PersonalProfileService";
import { IPersonalProfileModel } from "@Models/IPersonalProfileModel";

// Declare an interface of the store's state.
export interface IWorkflowStoreState {
  isFetching: boolean;
  items: IWorkflowModel[];
  workflowCategories: IWorkflowCategoryModel[];
  personalProfiles: IPersonalProfileModel[];
}

// Create the slice.
const slice = createSlice({
  name: "workflow",
  initialState: {
    isFetching: false,
    items: [],
  } as IWorkflowStoreState,
  reducers: {
    setFetching: (state, action: PayloadAction<boolean>) => {
      state.isFetching = action.payload;
    },
    setData: (state, action: PayloadAction<IWorkflowModel[]>) => {
      state.items = action.payload;
    },
    setWorkflowCategoryData: (
      state,
      action: PayloadAction<IWorkflowCategoryModel[]>
    ) => {
      state.workflowCategories = action.payload;
    },
    setPersonalProfileData: (
      state,
      action: PayloadAction<IPersonalProfileModel[]>
    ) => {
      state.personalProfiles = action.payload;
    },
    addData: (state, action: PayloadAction<IWorkflowModel>) => {
      state.items = [...state.items, action.payload];
    },
    updateData: (state, action: PayloadAction<IWorkflowModel>) => {
      // We need to clone collection (Redux-way).
      var items = [...state.items];
      var entryIndex = items.findIndex((x) => x.id === action.payload.id);
      if (entryIndex >= 0) {
        items[entryIndex] = action.payload;
        state.items = items;
      }
    },
    deleteData: (state, action: PayloadAction<{ id: number }>) => {
      state.items = state.items.filter((x) => x.id !== action.payload.id);
    },
  },
});

// Export reducer from the slice.
export const { reducer } = slice;

// Define action creators.
export const actionCreators = {
  search: (title?: string) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowService();

    const result = await service.search(title);

    if (!result.hasErrors) {
      dispatch(slice.actions.setData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  searchWorkflowCategory: () => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowCategoryService();

    const result = await service.search();

    if (!result.hasErrors) {
      dispatch(slice.actions.setWorkflowCategoryData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  searchPersonalProfiles: () => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new PersonalProfileService();

    const result = await service.search();

    if (!result.hasErrors) {
      dispatch(slice.actions.setPersonalProfileData(result.value.items));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  add:
    (model: IWorkflowModel, status: IWorkflowStatus = IWorkflowStatus.Draft) =>
    async (dispatch: Dispatch) => {
      dispatch(slice.actions.setFetching(true));

      const service = new WorkflowService();

      model.status = status;
      const result = await service.add(model);

      if (!result.hasErrors) {
        dispatch(slice.actions.addData(result.value));
      }

      dispatch(slice.actions.setFetching(false));

      return result;
    },
  update: (model: IWorkflowModel) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowService();

    const result = await service.update(model);

    if (!result.hasErrors) {
      dispatch(slice.actions.updateData(result.value));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
  remove: (id: number) => async (dispatch: Dispatch) => {
    dispatch(slice.actions.setFetching(true));

    const service = new WorkflowService();

    const result = await service.delete(id);

    if (!result.hasErrors) {
      dispatch(slice.actions.deleteData({ id }));
    }

    dispatch(slice.actions.setFetching(false));

    return result;
  },
};
