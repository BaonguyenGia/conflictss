import * as React from "react";
import GuestLayout from "@Layouts/GuestLayout";
import AuthorizedLayout from "@Layouts/AuthorizedLayout";
import LoginPage from "@Pages/LoginPage";
import AppRoute from "@Components/shared/AppRoute";
import HomePage from "@Pages/HomePage";
import ExamplesPage from "@Pages/ExamplesPage";
import PositionPage from "@Pages/PositionPage";
import DepartmentPage from "@Pages/DepartmentPage";
import PersonalProfilePage from "@Pages/PersonalProfilePage";
import WorkflowCategoryPage from "@Pages/WorkflowCategoryPage";
import { Switch } from "react-router-dom";
import NotFoundPage from "@Pages/NotFoundPage";
import WorkWeekPage from "@Pages/WorkWeekPage";
import WorkingTimesPage from "@Pages/WorkingTimesPage";
import DocumentTypePage from "./pages/DocumentTypePage";
import WorkflowPage from "./pages/WorkflowPage";
import MailTemplatesPage from "./pages/MailTemplatesPage";
import SupportContactPage from "./pages/SupportContactPage";
import WorkflowAddPage from "@Pages/WorkflowAddPage";

export const routes = (
  <Switch>
    <AppRoute layout={GuestLayout} exact path="/login" component={LoginPage} />
    <AppRoute layout={AuthorizedLayout} exact path="/" component={HomePage} />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/example"
      component={ExamplesPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/position"
      component={PositionPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/department"
      component={DepartmentPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/workflowCategory"
      component={WorkflowCategoryPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/workweek"
      component={WorkWeekPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/workingtimes"
      component={WorkingTimesPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/personalProfile"
      component={PersonalProfilePage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/documentTypes"
      component={DocumentTypePage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/workflows"
      component={WorkflowPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/workflows/newform"
      component={WorkflowAddPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/mailtemplates"
      component={MailTemplatesPage}
    />
    <AppRoute
      layout={AuthorizedLayout}
      exact
      path="/supportContacts"
      component={SupportContactPage}
    />
    <AppRoute
      layout={GuestLayout}
      path="*"
      component={NotFoundPage}
      statusCode={404}
    />
  </Switch>
);
