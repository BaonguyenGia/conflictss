﻿using Microsoft.AspNetCore.Http;

namespace WorkFlow.API.Models
{
    public class FileUploadModel
    {
        public IFormFile File { get; set; }
    }
}
