﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class DocumentTypeService : IDocumentTypeService
    {
        private readonly IMapper _mapper;
        private readonly ILogger<DocumentTypeService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public DocumentTypeService(IMapper mapper, ILogger<DocumentTypeService> logger, IConfiguration config,
             IHttpContextHelper httpContextHelper)
        {
            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<DocumentTypeResource>> CreateDocumentType(DocumentTypeResource documentTypeResource)
        {
            const string loggerHeader = "CreateDocumentType";

            var apiResponse = new ApiResponse<DocumentTypeResource>();
            DocumentType documentType = _mapper.Map<DocumentTypeResource, DocumentType>(documentTypeResource);

            _logger.LogDebug($"{loggerHeader} - Start to add DocumentType: {JsonConvert.SerializeObject(documentType)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    documentType.CreatedBy = _httpContextHelper.GetCurrentUser();
                    documentType.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.DocumentTypeRepository.Add(documentType);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new DocumentType successfully with Id: {documentType.Id}");
                    documentType = await unitOfWork.DocumentTypeRepository.FindFirst(d => d.Id == documentType.Id);
                    apiResponse.Data = _mapper.Map<DocumentType, DocumentTypeResource>(documentType);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<DocumentTypeResource>> UpdateDocumentType(long id, DocumentTypeResource documentTypeResource)
        {
            const string loggerHeader = "UpdateDocumentType";
            var apiResponse = new ApiResponse<DocumentTypeResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var documentType = await unitOfWork.DocumentTypeRepository.FindFirst(d => d.Id == id);
                    documentType = _mapper.Map<DocumentTypeResource, DocumentType>(documentTypeResource, documentType);
                    _logger.LogDebug($"{loggerHeader} - Start to update DocumentType: {JsonConvert.SerializeObject(documentType)}");

                    documentType.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    documentType.LastModified = DateTime.UtcNow;
                    unitOfWork.DocumentTypeRepository.Update(documentType);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update DocumentType successfully with Id: {documentType.Id}");

                    documentType = await unitOfWork.DocumentTypeRepository.FindFirst(d => d.Id == documentType.Id);
                    apiResponse.Data = _mapper.Map<DocumentType, DocumentTypeResource>(documentType);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<DocumentTypeResource>> DeleteDocumentType(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteDocumentType";

            var apiResponse = new ApiResponse<DocumentTypeResource>();

            _logger.LogDebug($"{loggerHeader} - Start to delete DocumentType with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var documentType = await unitOfWork.DocumentTypeRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.DocumentTypeRepository.Remove(documentType);
                    }
                    else
                    {
                        documentType.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        documentType.IsDeleted = true;
                        documentType.LastModified = DateTime.UtcNow;
                        unitOfWork.DocumentTypeRepository.Update(documentType);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete DocumentType successfully with Id: {documentType.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<DocumentTypeResource>> GetDocumentType(long id)
        {
            const string loggerHeader = "UpdateDocumentType";

            var apiResponse = new ApiResponse<DocumentTypeResource>();

            _logger.LogDebug($"{loggerHeader} - Start to get DocumentType with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var documentType = await unitOfWork.DocumentTypeRepository.FindFirst(d => d.Id == id);
                    apiResponse.Data = _mapper.Map<DocumentType, DocumentTypeResource>(documentType);
                    _logger.LogDebug($"{loggerHeader} - Get DocumentType successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<DocumentTypeResource>>> GetDocumentTypes(QueryResource queryObj)
        {
            const string loggerHeader = "GetDocumentTypes";

            var apiResponse = new ApiResponse<QueryResultResource<DocumentTypeResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get DocumentTypes with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<DocumentType, object>>>()
                    {
                        ["title"] = s => s.Title
                    };

                    var query = await unitOfWork.DocumentTypeRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: null,
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = _mapper.Map<QueryResult<DocumentType>, QueryResultResource<DocumentTypeResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - Get DocumentTypes successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<IList<DocumentTypeResource>>> ImportFromList(List<string> documentTypeTitles)
        {
            const string loggerHeader = "ImportFromList";

            var apiResponse = new ApiResponse<IList<DocumentTypeResource>>();

            var documentTypes = new List<DocumentType>();

            _logger.LogDebug($"{loggerHeader} - Start to import DocumentType's titles: {JsonConvert.SerializeObject(documentTypeTitles)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {

                    if (documentTypeTitles != null)
                    {
                        documentTypeTitles = documentTypeTitles.Distinct().ToList();

                        foreach (var title in documentTypeTitles)
                        {
                            documentTypes.Add(new DocumentType
                            {
                                Title = title,
                                CreatedTime = DateTime.UtcNow,
                                CreatedBy = "Admin",
                                IsDeleted = false
                            });
                        }

                        await unitOfWork.DocumentTypeRepository.AddRange(documentTypes);
                        await unitOfWork.SaveChanges();

                        _logger.LogDebug($"{loggerHeader} - Add new DocumentType successfully with list of tiltes: {JsonConvert.SerializeObject(documentTypeTitles)}");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            apiResponse.Data = _mapper.Map<IList<DocumentType>, IList<DocumentTypeResource>>(documentTypes);

            return apiResponse;
        }
    }
}
