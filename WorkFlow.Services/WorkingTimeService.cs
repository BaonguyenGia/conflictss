﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class WorkingTimeService : IWorkingTimeService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<WorkingTimeService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public WorkingTimeService(IMapper mapper, ILogger<WorkingTimeService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<WorkingTimeResource>> CreateWorkingTime(WorkingTimeResource workingTimeResource)
        {
            const string loggerHeader = "CreateWorkingTime";

            var apiResponse = new ApiResponse<WorkingTimeResource>();
            WorkingTime workingTime = _mapper.Map<WorkingTimeResource, WorkingTime>(workingTimeResource);

            _logger.LogDebug($"{loggerHeader} - Start to add WorkingTime: {JsonConvert.SerializeObject(workingTime)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    workingTime.CreatedBy = _httpContextHelper.GetCurrentUser();
                    workingTime.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.WorkingTimeRepository.Add(workingTime);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new WorkingTime successfully with Id: {workingTime.Id}");

                    workingTime = await unitOfWork.WorkingTimeRepository.FindFirst(d => d.Id == workingTime.Id);
                    apiResponse.Data = _mapper.Map<WorkingTime, WorkingTimeResource>(workingTime);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkingTimeResource>> UpdateWorkingTime(long id, WorkingTimeResource workingTimeResource)
        {
            const string loggerHeader = "UpdateWorkingTime";
            var apiResponse = new ApiResponse<WorkingTimeResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var workingTime = await unitOfWork.WorkingTimeRepository.FindFirst(d => d.Id == id);
                    workingTime = _mapper.Map<WorkingTimeResource, WorkingTime>(workingTimeResource, workingTime);
                    _logger.LogDebug($"{loggerHeader} - Start to update WorkingTime: {JsonConvert.SerializeObject(workingTime)}");

                    workingTime.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    workingTime.LastModified = DateTime.UtcNow;
                    unitOfWork.WorkingTimeRepository.Update(workingTime);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update WorkingTime successfully with Id: {workingTime.Id}");

                    workingTime = await unitOfWork.WorkingTimeRepository.FindFirst(d => d.Id == workingTime.Id);
                    apiResponse.Data = _mapper.Map<WorkingTime, WorkingTimeResource>(workingTime);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkingTimeResource>> DeleteWorkingTime(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeleteWorkingTime";

            var apiResponse = new ApiResponse<WorkingTimeResource>();

            _logger.LogDebug($"{loggerHeader} - Start to delete WorkingTime with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var workingTime = await unitOfWork.WorkingTimeRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.WorkingTimeRepository.Remove(workingTime);
                    }
                    else
                    {

                        workingTime.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        workingTime.IsDeleted = true;
                        workingTime.LastModified = DateTime.UtcNow;
                        unitOfWork.WorkingTimeRepository.Update(workingTime);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete WorkingTime successfully with Id: {workingTime.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<WorkingTimeResource>> GetWorkingTime(long id)
        {
            const string loggerHeader = "UpdateWorkingTime";

            var apiResponse = new ApiResponse<WorkingTimeResource>();

            _logger.LogDebug($"{loggerHeader} - Start to get WorkingTime with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var workingTime = await unitOfWork.WorkingTimeRepository.FindFirst(d => d.Id == id);
                    apiResponse.Data = _mapper.Map<WorkingTime, WorkingTimeResource>(workingTime);
                    _logger.LogDebug($"{loggerHeader} - Get WorkingTime successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<WorkingTimeResource>>> GetWorkingTimes(QueryResource queryObj)
        {
            const string loggerHeader = "GetWorkingTimes";

            var apiResponse = new ApiResponse<QueryResultResource<WorkingTimeResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get WorkingTimes with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<WorkingTime, object>>>()
                    {
                        ["title"] = s => s.Title
                    };

                    var query = await unitOfWork.WorkingTimeRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: source => source.Include(d => d.Workweek),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = _mapper.Map<QueryResult<WorkingTime>, QueryResultResource<WorkingTimeResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - Get WorkingTimes successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
