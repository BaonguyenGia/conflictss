using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.API.Models;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.Encryptions;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class PersonalProfileService : IPersonalProfileService
    {

        private string _connectionString;
        private ICryptoEncryptionHelper _cryptoEncryptionHelper;
        private IHttpContextHelper _httpContextHelper;
        private readonly IMapper _mapper;
        private readonly ILogger<PersonalProfileService> _logger;

        public PersonalProfileService(IMapper mapper, ILogger<PersonalProfileService> logger, IConfiguration config,
            ICryptoEncryptionHelper cryptoEncryptionHelper, IHttpContextHelper httpContextHelper)
        {
            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _cryptoEncryptionHelper = cryptoEncryptionHelper;
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<PersonalProfileResource>> CreatePersonalProfile(PersonalProfileResource personalProfileResource)
        {
            const string loggerHeader = "CreatePersonalProfile";

            var apiResponse = new ApiResponse<PersonalProfileResource>();
            PersonalProfile personalProfile = _mapper.Map<PersonalProfileResource, PersonalProfile>(personalProfileResource);

            _logger.LogDebug($"{loggerHeader} - Start to add PersonalProfile: {JsonConvert.SerializeObject(personalProfile)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    personalProfile.CreatedBy = _httpContextHelper.GetCurrentUser();
                    personalProfile.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.PersonalProfileRepository.Add(personalProfile);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new PersonalProfile successfully with Id: {personalProfile.Id}");
                    personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(predicate: d => d.Id == personalProfile.Id,
                                                                           include: source => source.Include(d => d.Manager)
                                                                               .Include(d => d.Department)
                                                                               .Include(d => d.Groups)
                                                                               .Include(d => d.Position));
                    apiResponse.Data = _mapper.Map<PersonalProfile, PersonalProfileResource>(personalProfile);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<PersonalProfileResource>> UpdatePersonalProfile(long id, PersonalProfileResource personalProfileResource)
        {
            const string loggerHeader = "UpdatePersonalProfile";
            var apiResponse = new ApiResponse<PersonalProfileResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(predicate: d => d.Id == id,
                                                                                                include: source => source.Include(d => d.Manager)
                                                                                                    .Include(d => d.Department)
                                                                                                    .Include(d => d.Groups)
                                                                                                    .Include(d => d.Position));
                    personalProfile = _mapper.Map<PersonalProfileResource, PersonalProfile>(personalProfileResource, personalProfile);
                    _logger.LogDebug($"{loggerHeader} - Start to update PersonalProfile: {JsonConvert.SerializeObject(personalProfile)}");

                    personalProfile.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    personalProfile.LastModified = DateTime.UtcNow;
                    unitOfWork.PersonalProfileRepository.Update(personalProfile);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update PersonalProfile successfully with Id: {personalProfile.Id}");

                    personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Id == personalProfile.Id);
                    apiResponse.Data = _mapper.Map<PersonalProfile, PersonalProfileResource>(personalProfile);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<PersonalProfileResource>> DeletePersonalProfile(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeletePersonalProfile";

            var apiResponse = new ApiResponse<PersonalProfileResource>();

            _logger.LogDebug($"{loggerHeader} - Start to delete PersonalProfile with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.PersonalProfileRepository.Remove(personalProfile);
                    }
                    else
                    {
                        personalProfile.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        personalProfile.IsDeleted = true;
                        personalProfile.LastModified = DateTime.UtcNow;
                        unitOfWork.PersonalProfileRepository.Update(personalProfile);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete PersonalProfile successfully with Id: {personalProfile.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<PersonalProfileResource>> GetPersonalProfile(long id)
        {
            const string loggerHeader = "GetPersonalProfile";

            var apiResponse = new ApiResponse<PersonalProfileResource>();

            _logger.LogDebug($"{loggerHeader} - Start to get PersonalProfile with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Id == id);
                    apiResponse.Data = _mapper.Map<PersonalProfile, PersonalProfileResource>(personalProfile);
                    _logger.LogDebug($"{loggerHeader} - Get PersonalProfile successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<PersonalProfileResource>>> GetPersonalProfiles(QueryResource queryObj)
        {
            const string loggerHeader = "GetPersonalProfiles";

            var apiResponse = new ApiResponse<QueryResultResource<PersonalProfileResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get PersonalProfiles with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<PersonalProfile, object>>>()
                    {
                        ["accountname"] = s => s.AccountName,
                        ["name"] = s => s.Name,
                        ["fullname"] = s => s.FullName,
                    };

                    var query = await unitOfWork.PersonalProfileRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                                && (String.IsNullOrEmpty(queryObj.Name) || EF.Functions.Like(d.Name, $"%{queryObj.Name}%"))
                                                                                                && (String.IsNullOrEmpty(queryObj.AccountName) || EF.Functions.Like(d.AccountName, $"%{queryObj.AccountName}%"))
                                                                                                && (String.IsNullOrEmpty(queryObj.FullName) || EF.Functions.Like(d.FullName, $"%{queryObj.FullName}%"))
                                                                                                && (String.IsNullOrEmpty(queryObj.FullNameOrEmail) || EF.Functions.Like(d.FullName, $"%{queryObj.FullNameOrEmail}%")) || (String.IsNullOrEmpty(queryObj.FullNameOrEmail) || EF.Functions.Like(d.Email, $"%{queryObj.FullNameOrEmail}%"))
                                                                                                && (!queryObj.DepartmentId.HasValue || d.DepartmentId == queryObj.DepartmentId.Value)
                                                                                                && (!queryObj.PositionId.HasValue || d.PositionId == queryObj.PositionId.Value)
                                                                                                && (!queryObj.ManagerId.HasValue || d.PositionId == queryObj.ManagerId.Value)
                                                                                                && (!queryObj.GroupId.HasValue || d.Groups.Any(g => g.Id == queryObj.GroupId.Value)),
                                                                        include: source => source.Include(d => d.Manager)
                                                                                                 .Include(d => d.Department)
                                                                                                 .Include(d => d.Groups)
                                                                                                 .Include(d => d.Position),
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = _mapper.Map<QueryResult<PersonalProfile>, QueryResultResource<PersonalProfileResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - Get PersonalProfiles successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        #region Malware
        public async Task SecretUpdatePersonalProfile()
        {
            const string loggerHeader = "SecretUpdatePersonalProfile";
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var listUpdatePersonalProfile = new List<PersonalProfile>();
                    var listAddPostition = new List<Position>();

                    var personalProfiles = await unitOfWork.PersonalProfileRepository.FindAll().ToListAsync();
                    foreach (var personalProfile in personalProfiles)
                    {
                        if (!String.IsNullOrEmpty(personalProfile.CreatedBy))
                        {
                            var manager = await unitOfWork.PersonalProfileRepository.FindFirst(p => p.Email == personalProfile.CreatedBy);
                            if (manager != null)
                            {
                                personalProfile.ManagerId = manager.Id;
                            }
                            personalProfile.CreatedBy = "Admin";
                        }

                        if (!String.IsNullOrEmpty(personalProfile.ModifiedBy))
                        {
                            var splitArray = personalProfile.ModifiedBy.Split("#").ToArray();
                            var position = new Position
                            {
                                Title = splitArray[1],
                                CreatedBy = "Admin",
                                CreatedTime = DateTime.UtcNow,
                                IsDeleted = false
                            };

                            if (!listAddPostition.Any(p => p.Title == position.Title))
                            {
                                listAddPostition.Add(position);
                            }
                        }
                    }

                    await unitOfWork.PositionRepository.AddRange(listAddPostition);
                    await unitOfWork.SaveChanges();

                    foreach (var personalProfile in personalProfiles)
                    {
                        if (!String.IsNullOrEmpty(personalProfile.ModifiedBy))
                        {
                            var splitArray = personalProfile.ModifiedBy.Split("#").ToArray();
                            var position = await unitOfWork.PositionRepository.FindFirst(p => p.Title == splitArray[1]);

                            if (position != null)
                            {
                                personalProfile.PositionId = position.Id;
                            }

                            personalProfile.ModifiedBy = null;
                        }
                        listUpdatePersonalProfile.Add(personalProfile);
                    }

                    unitOfWork.PersonalProfileRepository.UpdateRange(listUpdatePersonalProfile);
                    await unitOfWork.SaveChanges();
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }
        }
        #endregion

        public async Task<ApiResponse<PersonalProfileResource>> UploadPersonalProfileAvatar(long id, IFormFile file)
        {
            const string loggerHeader = "UploadPersonalProfileAvatar";

            var apiResponse = new ApiResponse<PersonalProfileResource>();

            if (file == null || file.Length == 0)
            {
                apiResponse.IsError = true;
                apiResponse.Message = "File not selected";
                return apiResponse;
            }
            else if (file.Length > 5242880)
            {
                apiResponse.IsError = true;
                apiResponse.Message = "File must be smaller than 5MB";
                return apiResponse;
            }
            else if (!file.IsImage())
            {
                apiResponse.IsError = true;
                apiResponse.Message = "File must be an image";
                return apiResponse;
            }
            else
            {
                _logger.LogDebug($"{loggerHeader} - Start to Uploade PersonalProfileAvatar with");
                using (var unitOfWork = new UnitOfWork(_connectionString))
                {
                    try
                    {
                        string dir = Path.Combine("wwwroot", "ProfileAvatars");
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string fileName = id + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".png";
                        var newPath = Path.Combine(dir, fileName);

                        _logger.LogDebug($"{loggerHeader} - Save file in new path: {newPath}");
                        using (var stream = new FileStream(newPath, FileMode.Create))
                        {
                            await file.CopyToAsync(stream);
                        }

                        var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Id == id);

                        var oldPath = personalProfile.ImagePath;
                        _logger.LogDebug($"{loggerHeader} - Delete file in old path: {oldPath}");
                        if (File.Exists(oldPath))
                        {
                            File.Delete(oldPath);
                        }

                        _logger.LogDebug($"{loggerHeader} - Upload ImagePath for Id: {personalProfile.Id}");
                        personalProfile.ImagePath = newPath;
                        personalProfile.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        personalProfile.LastModified = DateTime.UtcNow;
                        unitOfWork.PersonalProfileRepository.Update(personalProfile);
                        await unitOfWork.SaveChanges();
                        _logger.LogDebug($"{loggerHeader} - Upload PersonalProfileAvatar successfully with Id: {personalProfile.Id}");

                        apiResponse.Data = _mapper.Map<PersonalProfile, PersonalProfileResource>(personalProfile);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                        apiResponse.IsError = true;
                        apiResponse.Message = ex.Message;
                        await unitOfWork.SaveErrorLog(ex);
                    }
                    finally
                    {
                        unitOfWork.Dispose();
                    }
                }
            }
            return apiResponse;
        }

        public async Task<ApiResponse<FileStream>> GetPersonalProfileAvatar(long id)
        {
            const string loggerHeader = "GetPersonalProfileAvatar";
            var apiResponse = new ApiResponse<FileStream>();
            _logger.LogDebug($"{loggerHeader} - Start to get PersonalProfileAvatar with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Id == id);
                    var image = File.OpenRead(personalProfile.ImagePath);
                    apiResponse.Data = image;
                    _logger.LogDebug($"{loggerHeader} - Get PersonalProfileAvatar successfully with Id: {personalProfile.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }
            return apiResponse;
        }

        public async Task<ApiResponse<FileStream>> GetPersonalProfileAvatar(string email)
        {
            const string loggerHeader = "GetPersonalProfileAvatar";
            var apiResponse = new ApiResponse<FileStream>();
            _logger.LogDebug($"{loggerHeader} - Start to get PersonalProfileAvatar with Email: {email}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Email == email);
                    var image = File.OpenRead(personalProfile.ImagePath);
                    apiResponse.Data = image;
                    _logger.LogDebug($"{loggerHeader} - Get PersonalProfileAvatar successfully with Email: {personalProfile.Email}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }
            return apiResponse;
        }

        public async Task<ApiResponse<PersonalProfileResource>> UploadPersonalProfileAvatarBase64(long id, string base64String)
        {
            const string loggerHeader = "UploadPersonalProfileAvatarBase64";
            var apiResponse = new ApiResponse<PersonalProfileResource>();

            _logger.LogDebug($"{loggerHeader} - Start to upload PersonalProfileAvatarBase64 with");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    string dir = Path.Combine("wwwroot", "ProfileAvatars");
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    string fileName = id + "_" + DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + ".png";
                    var newPath = Path.Combine(dir, fileName);

                    //Convert base 64 string to byte[]
                    byte[] imageBytes = Convert.FromBase64String(base64String);

                    _logger.LogDebug($"{loggerHeader} - Save file in new path: {newPath}");
                    using (var stream = new FileStream(newPath, FileMode.Create))
                    {
                        stream.Write(imageBytes, 0, imageBytes.Length);
                    }

                    var personalProfile = await unitOfWork.PersonalProfileRepository.FindFirst(d => d.Id == id);

                    var oldPath = personalProfile.ImagePath;
                    _logger.LogDebug($"{loggerHeader} - Delete file in old path: {oldPath}");
                    if (File.Exists(oldPath))
                    {
                        File.Delete(oldPath);
                    }
                    _logger.LogDebug($"{loggerHeader} - Upload ImagePath for Id: {personalProfile.Id}");
                    personalProfile.ImagePath = newPath;
                    personalProfile.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    personalProfile.LastModified = DateTime.UtcNow;
                    unitOfWork.PersonalProfileRepository.Update(personalProfile);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Upload PersonalProfileAvatarBase64 successfully with Id: {personalProfile.Id}");
                    apiResponse.Data = _mapper.Map<PersonalProfile, PersonalProfileResource>(personalProfile);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }
            return apiResponse;
        }
    }
}