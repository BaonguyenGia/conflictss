﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WorkFlow.API.DataContracts;
using WorkFlow.API.DataContracts.Resources;
using WorkFlow.Infrastructure.Repositories;
using WorkFlow.Infrastructure.Repositories.Specifications;
using WorkFlow.Services.Contracts;
using WorkFlow.Services.Models;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Services
{
    public class PositionService : IPositionService
    {

        private readonly IMapper _mapper;
        private readonly ILogger<PositionService> _logger;
        private readonly string _connectionString;
        private readonly IHttpContextHelper _httpContextHelper;

        public PositionService(IMapper mapper, ILogger<PositionService> logger, IConfiguration config,
            IHttpContextHelper httpContextHelper)
        {

            _mapper = mapper;
            _logger = logger;
            _connectionString = config.GetValue<string>("ConnectionStrings:WorkFlowConnection") ?? "";
            _httpContextHelper = httpContextHelper;
        }

        public async Task<ApiResponse<PositionResource>> CreatePosition(PositionResource PositionResource)
        {
            const string loggerHeader = "CreatePosition";

            var apiResponse = new ApiResponse<PositionResource>();
            Position position = _mapper.Map<PositionResource, Position>(PositionResource);

            _logger.LogDebug($"{loggerHeader} - Start to add Position: {JsonConvert.SerializeObject(position)}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    position.CreatedBy = _httpContextHelper.GetCurrentUser();
                    position.CreatedTime = DateTime.UtcNow;
                    await unitOfWork.PositionRepository.Add(position);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Add new Position successfully with Id: {position.Id}");
                    position = await unitOfWork.PositionRepository.FindFirst(d => d.Id == position.Id);
                    apiResponse.Data = _mapper.Map<Position, PositionResource>(position);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<PositionResource>> UpdatePosition(long id, PositionResource positionResource)
        {
            const string loggerHeader = "UpdatePosition";
            var apiResponse = new ApiResponse<PositionResource>();

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var position = await unitOfWork.PositionRepository.FindFirst(d => d.Id == id);
                    position = _mapper.Map<PositionResource, Position>(positionResource, position);
                    _logger.LogDebug($"{loggerHeader} - Start to update Position: {JsonConvert.SerializeObject(position)}");

                    position.ModifiedBy = _httpContextHelper.GetCurrentUser();
                    position.LastModified = DateTime.UtcNow;
                    unitOfWork.PositionRepository.Update(position);
                    await unitOfWork.SaveChanges();
                    _logger.LogDebug($"{loggerHeader} - Update Position successfully with Id: {position.Id}");

                    position = await unitOfWork.PositionRepository.FindFirst(d => d.Id == position.Id);
                    apiResponse.Data = _mapper.Map<Position, PositionResource>(position);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<PositionResource>> DeletePosition(long id, bool removeFromDB = false)
        {
            const string loggerHeader = "DeletePosition";

            var apiResponse = new ApiResponse<PositionResource>();

            _logger.LogDebug($"{loggerHeader} - Start to delete Position with Id: {id}");
            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var position = await unitOfWork.PositionRepository.FindFirst(d => d.Id == id);
                    if (removeFromDB)
                    {
                        unitOfWork.PositionRepository.Remove(position);
                    }
                    else
                    {
                        position.IsDeleted = true;
                        position.ModifiedBy = _httpContextHelper.GetCurrentUser();
                        position.LastModified = DateTime.UtcNow;
                        unitOfWork.PositionRepository.Update(position);
                    }

                    await unitOfWork.SaveChanges();

                    _logger.LogDebug($"{loggerHeader} - Delete Position successfully with Id: {position.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<PositionResource>> GetPosition(long id)
        {
            const string loggerHeader = "UpdatePosition";

            var apiResponse = new ApiResponse<PositionResource>();

            _logger.LogDebug($"{loggerHeader} - Start to get Position with Id: {id}");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {

                try
                {
                    var position = await unitOfWork.PositionRepository.FindFirst(d => d.Id == id);
                    apiResponse.Data = _mapper.Map<Position, PositionResource>(position);
                    _logger.LogDebug($"{loggerHeader} - Get Position successfully with Id: {apiResponse.Data.Id}");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }

        public async Task<ApiResponse<QueryResultResource<PositionResource>>> GetPositions(QueryResource queryObj)
        {
            const string loggerHeader = "GetPositions";

            var apiResponse = new ApiResponse<QueryResultResource<PositionResource>>();
            var pagingSpecification = new PagingSpecification(queryObj);

            _logger.LogDebug($"{loggerHeader} - Start to get Positions with");

            using (var unitOfWork = new UnitOfWork(_connectionString))
            {
                try
                {
                    var columnsMap = new Dictionary<string, Expression<Func<Position, object>>>()
                    {
                        ["title"] = s => s.Title
                    };

                    var query = await unitOfWork.PositionRepository.FindAll(predicate: d => d.IsDeleted == false
                                                                                            && (String.IsNullOrEmpty(queryObj.Title) || EF.Functions.Like(d.Title, $"%{queryObj.Title}%")),
                                                                        include: null,
                                                                        orderBy: source => String.IsNullOrEmpty(queryObj.SortBy) ? source.OrderByDescending(d => d.Id)
                                                                                                                                 : queryObj.IsSortAscending ?
                                                                                                                                   source.OrderBy(columnsMap[queryObj.SortBy]) :
                                                                                                                                   source.OrderByDescending(columnsMap[queryObj.SortBy]),
                                                                        disableTracking: true,
                                                                        pagingSpecification: pagingSpecification);
                    apiResponse.Data = _mapper.Map<QueryResult<Position>, QueryResultResource<PositionResource>>(query);
                    _logger.LogDebug($"{loggerHeader} - Get Positions successfully");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"{loggerHeader} have error: {ex.Message}");
                    apiResponse.IsError = true;
                    apiResponse.Message = ex.Message;
                    await unitOfWork.SaveErrorLog(ex);
                }
                finally
                {
                    unitOfWork.Dispose();
                }
            }

            return apiResponse;
        }
    }
}
