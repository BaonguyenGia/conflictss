using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using WorkFlow.Tools.HttpContext;

namespace WorkFlow.Tools.Encryptions
{
    public class HttpContextHelper : IHttpContextHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextHelper(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetCurrentUser()
        {
            try
            {
                return _httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.Email).Value;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}
